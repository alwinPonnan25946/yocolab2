///////////react native default imports
import React, { useEffect, useState, useContext } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, Pressable } from 'react-native'
////////////reuseable component import
////////////////global Styles
//////////////utilities like styles and images are imported here
////////////external packages which are installed are here
import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome5"
// import { colorObj } from '../../globals/colors';


import HomePage from '../../Views/HomePage';
import BecomeAnInstructor from '../../Views/BecomeAnInstructor';
import CreateClass from '../../Views/CreateClass';
import { getUserRole, getUserRoleData, removeJwt, removeRole } from '../../Services/user';

import { authContext } from '../Stack/RootStack';
import { useIsFocused } from '@react-navigation/native';
import { getStudentData } from '../../Services/Student';
import { getInstructorData } from '../../Services/Instructor';
import Home from '../../Views/Home';
export default function HomeDrawer() {
    const Drawer = createDrawerNavigator();
    const [isAuthorized, setIsAuthorized] = useContext(authContext);

    const [teacherProfileData, setTeacherProfileData] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [studentData, setStudentData] = useState("");
    const [role, setRole] = useState("");




    const isFocused = useIsFocused()


    const Logout = async () => {
        await removeJwt()
        await removeRole()
        setIsAuthorized(false)
    }



    const getuserData = async () => {
        try {
            let role = await getUserRole()
            if (role) {
                setRole(role);
            }
            if (role && role == "teacher") {
                let res = await getInstructorData()
                if (res.status == 304 || res.status == 200) {
                    console.log(JSON.stringify(res.data.data, null, 2), "response")
                    setTeacherProfileData(res.data.data)
                    setIsLoading(false)
                }
            }
            else if (role) {
                let res = await getStudentData()
                if (res.status == 304 || res.status == 200) {
                    console.log(JSON.stringify(res.data.data, null, 2), "Student response")
                    setStudentData(res.data.data)
                    setIsLoading(false)
                }
            }
        }
        catch (err) {
            console.error(err)
        }
    }







    useEffect(() => {
        if (isFocused) {
            getuserData()
        }
    }, [isFocused])

    ////////////////////custom user drawer 
    function CustomDrawerContent(props) {
        return (
            <DrawerContentScrollView  {...props}>
                <View style={styles.flexRow}>
                    {
                        role == "teacher" &&
                        <View style={styles.profilePicContainer}>
                            <Image source={require('../../../assets/img/slider-1.jpg')} style={styles.profilePic} />
                        </View>
                    }
                    <View style={styles.flexColumn}>
                        <Text style={styles.greeting}>Welcome Back !</Text>
                        {
                            role == "teacher"
                                ?
                                <Text style={styles.userName}>{teacherProfileData?.name}</Text>
                                :
                                <Text style={styles.userName}>{studentData?.first_name} {studentData?.last_name}</Text>
                        }
                        {/* <Pressable style={styles.myAccountBtn}>
                            <Text style={styles.myAccountTxt}>My Account</Text>
                        </Pressable> */}
                    </View>
                </View>
                <View style={{ marginTop: 0, width: "100%", alignSelf: "center", display: "flex", flexDirection: "column", backgroundColor: "white", height: hp(77) }}>
                    <TouchableOpacity onPress={() => props.navigation.jumpTo('HomePage')} style={styles.DrawerItem}><Icon size={12} name="home" color="black" /><Text style={styles.drawerItemTxt}>Home</Text></TouchableOpacity>
                    {
                        role == "teacher"
                            ?
                            <TouchableOpacity onPress={() => props.navigation.jumpTo('CreateClass')} style={styles.DrawerItem}><Icon size={12} name="tv" color="black" /><Text style={styles.drawerItemTxt}>Create Class</Text></TouchableOpacity>
                            :
                            < TouchableOpacity onPress={() => props.navigation.jumpTo('BecomeAnInstructor')} style={styles.DrawerItem}><Icon size={12} name="chalkboard-teacher" color="black" /><Text style={styles.drawerItemTxt}>Become An Instructor</Text></TouchableOpacity>


                    }
                    <TouchableOpacity onPress={() => Logout()} style={styles.DrawerItem}><Icon size={12} name="running" color="black" /><Text style={styles.drawerItemTxt}>Logout</Text></TouchableOpacity>
                </View>
            </DrawerContentScrollView >
        );
    }






    return (
        <>
            <Drawer.Navigator drawerType="slide" drawerPosition={'left'} drawerContent={props => <CustomDrawerContent {...props} />}>
                <Drawer.Screen name="HomePage" initialParams={{ changePosition: "left" }} component={Home} />
                <Drawer.Screen name="CreateClass" initialParams={{ changePosition: "left" }} component={CreateClass} />
                <Drawer.Screen name="BecomeAnInstructor" initialParams={{ changePosition: "left" }} component={BecomeAnInstructor} />
            </Drawer.Navigator>
        </>
    )
}
const styles = StyleSheet.create({
    profilePic: {
        height: 80,
        width: 80,
        alignSelf: "center",
        borderRadius: 10
    },
    profilePicContainer: {
        height: 110,
        width: 110,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        borderRadius: 70,
        backgroundColor: "white"
    },
    greeting: {
        fontFamily: "Montserrat-Bold",
        fontSize: 16,
        textAlign: "center",
        color: "black"
    },
    userName: {
        fontFamily: "Montserrat-Regular",
        fontSize: 14,
        marginVertical: 15,
        textTransform: "capitalize",
        textAlign: "center",
        // color: "rgba(255,255,255,0.7)"
        color: "black"
    },
    DrawerItem: {
        display: "flex",
        flexDirection: "row",
        marginVertical: 10,
        alignItems: "center",
        paddingLeft: 15
    },
    drawerItemTxt: {
        marginLeft: 20,
        color: "black",
        fontSize: 17,
    },
    flexRow: {
        display: "flex",
        flexDirection: "row",
        paddingHorizontal: 10,
        height: hp(23)
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column",
        // backgroundColor: "red",
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    myAccountBtn: {
        backgroundColor: "rgba(0,0,0,0.2)",
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 5
    },
    myAccountTxt: {
        color: "white"
    },
})