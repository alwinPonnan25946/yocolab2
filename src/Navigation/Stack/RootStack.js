import React, { useState, useEffect, createContext, useMemo } from 'react'
import { View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';

////////components
import Login from "../../Views/Login";
import IntroScreen from "../../Views/IntroScreen";
import Register from "../../Views/Register";
import Home from "../../Views/Home";
import ForgotPassword from '../../Views/ForgotPassword';
import { getJwt, getUserRoleData, removeJwt } from '../../Services/user';
import BottomTabNavigator from '../Tab/BottomTabNavigator';
import axios from 'axios'
import Toast from 'react-native-toast-message';
import { useIsFocused } from '@react-navigation/native';
import EncryptedStorage from 'react-native-encrypted-storage';
import SpecificClass from '../../Views/SpecificClass';
import SpecificTeacher from '../../Views/SpecificTeacher';
import zoom from '../../Views/zoom';

export const authContext = createContext();

export default function RootStack() {
    const [isAuthorized, setIsAuthorized] = useState(false);



    const getUserRole = async () => {
        try {
            let res = await getUserRoleData()
            if (res.status == 200 || res.status == 304) {
                EncryptedStorage.setItem("role", res.data.role)
            }
        }
        catch (err) {
            console.error(err)
        }
    }


    useEffect(() => {
        getUserRole()
    }, [])


    const checkAuthorized = async () => {
        let token = await getJwt();
        if (token) {
            setIsAuthorized(true)
        }
        else {
            setIsAuthorized(false)
        }
    }

    useMemo(() => {
        axios.interceptors.request.use(
            async (config) => {
                const token = await getJwt();
                console.log(token)
                if (token) {
                    config.headers['authorization'] = 'Bearer ' + token;

                }
                // console.log(config)
                return config;
            },
            error => {
                Promise.reject(error)
            });
        axios.interceptors.response.use(
            (res) => {
                // Add configurations here

                return res;

            },
            async (err) => {
                if (err?.response?.status == 401) {

                    await removeJwt()
                    setIsAuthorized(false)

                }
                let str = ''
                if (err?.response?.data?.error) {
                    for (let key in err?.response?.data?.error) {
                        str = str + '\n' + `${key} : ${err?.response?.data?.error[key][0]}`
                    }
                    Toast.show({
                        text1: "Error Found",
                        text2: `${str}`,
                    });
                    alert()
                }
                return Promise.reject(err);
            }
        );
    }, [])



    useEffect(() => {
        checkAuthorized()
    }, [])


    const Stack = createStackNavigator();
    return (
        <authContext.Provider value={[isAuthorized, setIsAuthorized]}>
            <Stack.Navigator>
                {isAuthorized ?
                    <>

                        <Stack.Screen options={{ headerShown: false }} name="BottomTabNavigator" component={BottomTabNavigator} />
                        <Stack.Screen options={{ headerShown: false }} name="SpecificClass" component={SpecificClass} />
                        <Stack.Screen options={{ headerShown: false }} name="SpecificTeacher" component={SpecificTeacher} />
                        <Stack.Screen options={{ headerShown: false }} name="zoom" component={zoom} />


                    </>
                    :
                    <>
                        <Stack.Screen options={{ headerShown: false }} name="Login" component={Login} />
                        <Stack.Screen options={{ headerShown: false }} name="IntroScreen" component={IntroScreen} />
                        <Stack.Screen options={{ headerShown: false }} name="Register" component={Register} />
                        <Stack.Screen options={{ headerShown: false }} name="ForgotPassword" component={ForgotPassword} />
                        <Stack.Screen options={{ headerShown: false }} name="BottomTabNavigator" component={BottomTabNavigator} />
                    </>
                }
            </Stack.Navigator>
        </authContext.Provider>
    )
}
