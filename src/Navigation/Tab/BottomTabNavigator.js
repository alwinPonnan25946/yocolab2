///////////react native default imports
import React, { useEffect, useState } from 'react'
////////////external packages which are installed are here
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import HomeDrawer from '../Drawer/DrawerNavigator';
import TabBar from '../../Components/TabBar';
import Search from '../../Views/Search';
import Profile from '../../Views/Profile';
import WishList from '../../Views/WishList';
import Upcoming from '../../Views/Upcoming';
import { useIsFocused } from '@react-navigation/native';
import { getUserRoleData, setRoleToStorage } from '../../Services/user';

export default function BottomTabNavigator() {
    const Tab = createBottomTabNavigator();
    const isFocused = useIsFocused()
    const [role, setRole] = useState("");


    const getRole = async () => {
        let role = await getUserRoleData()
        if (role) {
            setRole(role.data.role)
            await setRoleToStorage(role.data.role);
        }

    }


    useEffect(() => {
        if (isFocused) {
            getRole()
        }
    }, [isFocused])


    return (
        <Tab.Navigator initialRouteName='HomeDrawer' tabBar={props => <TabBar {...props} />}>
            <Tab.Screen initialParams={{ icon: 'home' }} name='HomeDrawer' component={HomeDrawer} />
            <Tab.Screen initialParams={{ icon: 'search' }} name='Search' component={Search} />
            <Tab.Screen initialParams={{ icon: 'history' }} name='Upcoming' component={Upcoming} />
            <Tab.Screen initialParams={{ icon: 'heart' }} name='WishList' component={WishList} />
            {
                role == "teacher" &&
                <Tab.Screen initialParams={{ icon: 'user' }} name='Profile' component={Profile} />
            }
        </Tab.Navigator>
    )
}
