export default ColorObj = {
    ///////////////light theme
    lightThemeButtonColor: "#FD6003",//////orange
    lightThemeTextColor: "#6F7074",//////grey
    lightThemeBg1Color: "#0D1128",///////blue
    lightThemeBgBlackColor: "#151515",///////black
    lightThemeBgColor: "#fff",///////white
    lightThemeGreyBgColor: "#F5F6FA",///////grey



    ///////////////Dark theme
    darkThemeButtonColor: "",
    darkThemeTextColor: "",

}