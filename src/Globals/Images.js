export default Images = {
    intro1: require("../../assets/img/slider-1.jpg"),
    intro2: require("../../assets/img/slider-4.jpg"),
    intro3: require("../../assets/img/slider-7.jpg"),
    loader: require("../../assets/img/preloader.gif"),
    paid: require("../../assets/img/financial.png"),
    free: require("../../assets/img/free.png"),
}