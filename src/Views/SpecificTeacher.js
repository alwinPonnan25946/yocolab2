import React, { useState, useEffect, useContext } from 'react'
import { View, Text, StyleSheet, ScrollView, Pressable, Image, FlatList } from 'react-native'
import Header from "../Components/Header";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';
import { useIsFocused } from '@react-navigation/native';
import ColorObj from "../Globals/Colors";
import { nanoid } from 'nanoid/non-secure'
import { Chip } from 'react-native-paper';


import moment from 'moment';
import 'moment-timezone';

import { loadingContext } from '../../App'
import { getSpecificClassBySlug } from '../Services/Courses';
import Images from '../Globals/Images';

import CountDown from 'react-native-countdown-component';
import Colors from '../Globals/Colors';
import { addClassToWishlist } from '../Services/user';
import { getSpecificInstructor } from '../Services/Instructor';

import { WebView } from 'react-native-webview';

export default function SpecificTeacher(props) {


    const focused = useIsFocused()

    const [specificTeacherObj, setSpecificTeacherObj] = useState({});

    const [isLoading, setIsLoading] = useContext(loadingContext);
    const getSpecificClass = async () => {
        try {
            setIsLoading(true)
            let teacher_slug = props.route.params.slug
            console.log(teacher_slug)
            const { data: res } = await getSpecificInstructor(teacher_slug)
            if (res.success) {
                console.log(JSON.stringify(res.data, null, 2))
                let tempObj = res.data;

                console.log(tempObj)
                setSpecificTeacherObj(tempObj)

            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.error(error)
        }
    }

    const renderHtml = (content) => {
        return `
            <html>
            <head>
                <link rel="preconnect" href="https://fonts.googleapis.com">
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap" rel="stylesheet">
            </head>
            <body>
                <div style="height:500px;font-size:40px;font-family: 'Montserrat', sans-serif;background-color:'transparent';">${content}</div>
            </body>
            </html>
        `

    }


    const handleOninit = async () => {
        getSpecificClass()
    }

    const renderRatingStar=(rating)=>{
        let tempArr=[]
        for(let i=1;i<=5;i++){
            if(i<=rating){
               tempArr.push('<Icon name="star" size={16} color="black" />')
            }
            else{
                tempArr.push('<Icon name="star-outline" size={16} color="black" />')

            }
        }
        return tempArr
    }

    useEffect(() => {
        if (focused)
            handleOninit()
    }, [focused])



    return (
        <>

            <Header rootProps={props} name="Teacher" />
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.innerContainer}>
                    <View>
                        {specificTeacherObj?.image ?
                            <Image source={{ uri: specificTeacherObj?.image }} style={{ borderRadius: 10, height: 200, width: '100%', marginTop: 10, resizeMode: "cover" }} />
                            :
                            <Image source={Images.intro1} style={{ height: 200, width: '100%', marginTop: 10, resizeMode: "cover" }} />
                        }
                    </View>

                    <Text style={styles.mainHeading}>{specificTeacherObj?.name}</Text>
                    {/* <Text style={{textAlign:'center'}}>{renderRatingStar(specificTeacherObj?.rating).map(el=>{
                        return el
                    })}</Text> */}

                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }]}>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Students :</Text>  {specificTeacherObj?.students}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Rating :</Text> {specificTeacherObj?.rating}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Country :</Text> {specificTeacherObj?.country}</Text>

                    </View>
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between', marginTop: 10 }]}>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Since :</Text> {specificTeacherObj?.date?.split(' ')[0]}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Classes :</Text> {specificTeacherObj?.total_courses}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Language : </Text>{specificTeacherObj?.language}</Text>

                    </View>

                    <View>
                        <Pressable style={styles.btn}>
                            <Text style={styles.btnTxt}>Follow</Text>
                        </Pressable>
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <Text style={[styles.overviewSubheading, { marginTop: 10 }]}>Expertise</Text>

                        <Text style={styles.overviewText}>{specificTeacherObj?.expert}</Text>

                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.overviewSubheading}>Qualification</Text>
                        <Text style={styles.overviewText}>{specificTeacherObj?.qualification}</Text>


                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.overviewSubheading}>Experience</Text>
                        <Text style={styles.overviewText}>{specificTeacherObj?.experience}</Text>


                    </View>
                    <View style={{ marginTop: 20 }}>
                        <Text style={[styles.overviewSubheading, { marginTop: 10 }]}>About</Text>
                        {specificTeacherObj?.about &&
                        
                        <WebView
                        // originWhitelist={['*']}
                        source={{ html: renderHtml(specificTeacherObj?.about) }}
                        style={{ height:Math.ceil(specificTeacherObj?.about.split(' ').length/5)*16,width: '100%' }}
                        />
                    }

                    </View>
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    //////container
    container: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#FBFBFB",
        paddingBottom: 60,
    },
    innerContainer: {
        width: wp(90),
        marginHorizontal: 20,
        marginTop: 10
        // backgroundColor:'red',
        // height:hp(90)
    },
    mainHeading: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 24,
        textAlign: 'center'
    },
    mainSubHeading: {
        marginVertical: 10,
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
    }
    ,  //////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column"
    },
    underImageText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        paddingHorizontal: 5
    },
    btn: {
        width: '100%',
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    overviewHeading: {
        fontSize: 22,
        fontFamily: 'Montserrat-Bold',
        textDecorationLine: 'underline'
    },
    overviewSubheading: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        marginVertical: 5
    },
    overviewText: {
        fontFamily: 'Montserrat-Regular'
    }
})


