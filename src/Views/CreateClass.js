import React, { useEffect, useRef, useState } from 'react'
import { View, Text, StyleSheet, FlatList, Pressable, Image, ScrollView, Modal, TextInput } from 'react-native'
import Header from '../Components/Header';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Stepper from "react-native-stepper-ui";
import Colors from '../Globals/Colors';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import Images from '../Globals/Images';
import { Checkbox, Searchbar } from 'react-native-paper';
import Icon from "react-native-vector-icons/FontAwesome";
import DatePicker from 'react-native-date-picker';
import MultiSelect from 'react-native-multiple-select';
import DocumentPicker from 'react-native-document-picker';
import { nanoid } from 'nanoid/non-secure'
import { getAllCategories } from '../Services/Categories';
import { getAllCountries } from '../Services/Country';
import { getAllLanguages } from '../Services/Languages';
import { createCreateClass, getInstructor, getInstructorData, getStudyMaterialsInstructor, uploadStudyMaterialInstructor } from '../Services/Instructor';
import moment from 'moment';
import 'moment-timezone';

export default function CreateClass(props) {
    const multiselect = useRef();
    const [title, setTitle] = useState("");
    const [maximumStudents, setmaximumStudents] = useState(0);
    const [tag, setTag] = useState("");/////////tag value is stored here
    const [classDescription, setClassDescription] = useState("");
    const [date, setDate] = useState(new Date())
    const [studyMaterialTitle, setStudyMaterialTitle] = useState("");
    const [selectedItems, setSelectedItems] = useState();///////multiselect items
    const [studyMaterialFile, setStudyMaterialFile] = useState("");
    const [items, setItems] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState();
    const [classFee, setClassFee] = useState();
    const [discount, setDiscount] = useState();
    const [classImage, setClassImage] = useState("");
    const [youtubeUrl, setYoutubeUrl] = useState("");
    const [courseVideo, setCourseVideo] = useState("");
    const [currency, setCurrency] = useState("");



    ///////ref
    const maxStudentsRef = useRef();
    const tagsRef = useRef();
    const feeRef = useRef();
    const discountRef = useRef();


    ///////disable Steps States
    const [step2isDissabled, setStep2isDissabled] = useState(true);
    const [step3isDissabled, setStep3isDissabled] = useState(true);
    const [step4isDissabled, setStep4isDissabled] = useState(true);
    const [step5isDissabled, setStep5isDissabled] = useState(true);

    //////search querry
    const [searchQuery, setSearchQuery] = useState('');



    ////////main Arrays 
    const [classLevelArr, setClassLevelArr] = useState([
        { name: "Beginners", id: 1 },
        { name: "Intermediate", id: 2 },
        { name: "Advance", id: 3 },
    ]);


    const [languagesArr, setLanguagesArr] = useState([]);


    const [classCategoryArr, setClassCategoryArr] = useState([]);

    const [classSubjectArr, setClassSubjectArr] = useState([   ///////(this basically is subCategory)

    ]);
    const [learnArr, setLearnArr] = useState([{ value: "", id: nanoid() }]);
    const [requirementsArr, setRequirementsArr] = useState([
        { value: "", id: nanoid() },
    ]);
    const [tagsArr, setTagsArr] = useState([]);
    const [durationArr, setDurationArr] = useState([
        { duration: "00:30", value: 0.5 },
        { duration: "01:00", value: 1 },
        { duration: "01:30", value: 1.5 },
        { duration: "02:00", value: 2 },
        { duration: "02:30", value: 2.5 },
        { duration: "03:00", value: 3 },
        { duration: "03:30", value: 3.5 },
        { duration: "04:00", value: 4 },
    ]);

    const [slotsArr, setSlotsArr] = useState([
        { date: "", time: "", duration: "", durationValue: "", id: nanoid() },
    ]);

    const [studyMaterialArr, setStudyMaterialArr] = useState([]);

    const [finalSelectedItemsArr, setFinalSelectedItemsArr] = useState([]);





    //////filtered Arrays
    const [filteredLanguageArr, setFilteredLanguageArr] = useState([]);



    const [filteredClassCategoryArr, setFilteredClassCategoryArr] = useState([

    ]);
    const [filteredClassSubjectArr, setFilteredClassSubjectArr] = useState([
    ]);
    const [filteredStudyMaterialArr, setFilteredStudyMaterialArr] = useState([]);



    //////selected Values
    const [selectedClassLevel, setSelectedClassLevel] = useState("");
    const [selectedLanguage, setSelectedLanguage] = useState("");
    const [selectedClassCategory, setSelectedClassCategory] = useState("");
    const [selectedClassSubject, setSelectedClassSubject] = useState("");
    const [selectedCourseDuration, setSelectedCourseDuration] = useState("");
    const [selectedSlot, setSelectedSlot] = useState();
    const [selectedClassCategoryId, setSelectedClassCategoryId] = useState("");
    const [selectedsubjectId, setSelectedsubjectId] = useState("");
    const [selectedDurationValue, setSelectedDurationValue] = useState("");
    ///////modal Values
    const [classLevelModalVisible, setClassLevelModalVisible] = useState(false);
    const [languageModalVisible, setLanguageModalVisible] = useState(false);
    const [classCategoryModalVisible, setClassCategoryModalVisible] = useState(false);
    const [classSubjectModalVisible, setClassSubjectModalVisible] = useState(false);
    const [durationModalVisible, setDurationModalVisible] = useState(false);
    const [dateModalVisible, setdateModalVisible] = useState(false);
    const [timeModalVisible, setTimeModalVisible] = useState(false);
    const [studyMaterialModalVisible, setStudyMaterialModalVisible] = useState(false);


    const [tempArr, setTempArr] = useState([{ text: "", id: nanoid() }]);



    //////language
    const onChangeSearchLanguage = (val) => {
        setSearchQuery(val)
        let filteredArr = languagesArr.filter(el => el.name.toLowerCase().includes(val.toLowerCase()))
        setFilteredLanguageArr(filteredArr);
    }

    /////class category
    const onChangeSearchClassCategory = (val) => {
        setSearchQuery(val)
        let filteredArr = classCategoryArr.filter(el => el.name.toLowerCase().includes(val.toLowerCase()))
        setFilteredClassCategoryArr(filteredArr);
    }


    /////class subject
    const onChangeSearchClassSubject = (val) => {
        setSearchQuery(val)
        let filteredArr = classSubjectArr.filter(el => el.name.toLowerCase().includes(val.toLowerCase()))
        setFilteredClassSubjectArr(filteredArr);
    }




    //////tags
    const updateTagsArr = () => {

        if (tagsArr.length < 6) {
            setTagsArr(previousValue => {
                return [...previousValue, { name: tag, id: nanoid() }]
            })
        }
        else {
            alert("6 tags already entered please remove some to enter more")
        }
    }
    const deleteTags = (i) => {
        setTagsArr(previousValue => {
            previousValue.splice(i, 1);
            return [...previousValue]
        })
    }





    /////learn 
    const updateLearnTextValue = (val, i) => {
        setLearnArr(previousValue => {
            previousValue[i].value = val;
            return [...previousValue]
        })
    }
    const addLearnArrItem = () => {
        setLearnArr(previousValue => {
            previousValue.push({ value: "", id: nanoid() })
            return [...previousValue]
        })
    }
    const deleteLearnArrItem = (index) => {
        // console.log(learnArr[index])
        if (learnArr.length > 1) {

            setLearnArr(prevState => {
                prevState.splice(index, 1)
                return [...prevState]
            })
        }

    }






    /////////requirements
    const updateRequirementTextValue = (val, i) => {
        setRequirementsArr(previousValue => {
            previousValue[i].value = val;
            return [...previousValue]
        })
    }
    const addRequirementArrItem = () => {
        setRequirementsArr(previousValue => {
            return [...previousValue, { value: "", id: nanoid() }]
        })
    }
    const deleteRequirementArrItem = (index) => {
        if (requirementsArr.length > 1) {
            setRequirementsArr(prevState => {
                prevState.splice(index, 1)
                return [...prevState]
            })
        }
    }






    /////////slots
    const setSlotDate = (date) => {
        setSlotsArr(previousValue => {
            previousValue[selectedSlot].date = date
            return [...previousValue]
        })
    }
    const setSlotTime = (time) => {
        if (new Date(time).getMinutes() > 10) {
            let timeVal = `${new Date(time).getHours()}:${new Date(time).getMinutes()}`
            console.warn(timeVal)
            setSlotsArr(previousValue => {
                previousValue[selectedSlot].time = timeVal
                return [...previousValue]
            })

        }
        else {
            let timeVal = `${new Date(time).getHours()}:0${new Date(time).getMinutes()}`
            console.warn(timeVal)
            setSlotsArr(previousValue => {
                previousValue[selectedSlot].time = timeVal
                return [...previousValue]
            })

        }
    }
    const setDurationOfSlot = (duration, value) => {
        setSlotsArr(previousValue => {
            previousValue[selectedCourseDuration].duration = duration
            previousValue[selectedCourseDuration].durationValue = value
            return [...previousValue]
        })
    }
    const addSlots = () => {
        setSlotsArr(previousValue => {
            return [...previousValue, { date: "", time: "", duration: "", id: nanoid() }]
        })
    }
    const removeSlots = () => {
        if (slotsArr.length > 1) {

            setSlotsArr(previousState => {
                previousState.pop()
                return [...previousState]
            })
        }
    }





    const buttonTextStyle = {
        color: '#fff',
    };
    const BtnStyle = {
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: Colors.lightThemeButtonColor,
        borderRadius: 5
    }




    const onSelectedItemsChange = (selectedItems) => {
        setSelectedItems(selectedItems)
    }

    const setSelectedItemsArrValues = (val) => {
        let tempArr = items.filter(el => val.includes(el.id)).map(el => el.id);
        // console.log(tempArr, "temp Arr")
        // console.log(val, "value")
        setFinalSelectedItemsArr(tempArr)
    }





    const pickClassImage = async () => {
        try {
            // console.log("asdas")
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            setClassImage(res)
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const pickCoursePreview = async () => {
        try {
            // console.log("asdas")
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.video],
            });
            setCourseVideo(res)
            // console.log(res, "response")
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const pickImageStudyMaterial = async () => {
        try {
            // console.log("asdas")
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf, DocumentPicker.types.docx, DocumentPicker.types.csv, DocumentPicker.types.plainText, DocumentPicker.types.ppt, DocumentPicker.types.pptx, DocumentPicker.types.xls, DocumentPicker.types.xlsx],
            });
            setStudyMaterialFile(res)
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }


    const [classType, setClassType] = useState(0);




    const renderLearnFlatlist = ({ item, index }) => {
        return (

            <View style={styles.textInputContainer}>
                <TextInput style={[styles.TextInputStyles, { flex: 0.9 }]} value={item.value} onChangeText={(val) => { updateLearnTextValue(val, index); }} placeholder="e.g. Become a UX designer" />
                <Pressable onPress={() => addLearnArrItem()} style={styles.addBtnContainer}>
                    <Icon name="plus" color="green" size={22} />
                </Pressable>
                <Pressable onPress={() => deleteLearnArrItem(index)} style={styles.DeleteBtnContainer}>
                    <Icon name="minus" color="red" size={22} />
                </Pressable>
            </View>

        )
    }



    const setClassSubjectCategoryArrByClass = (item) => {
        setSelectedClassCategory(item.name)


        setClassSubjectArr(item.subcategory)
        setFilteredClassSubjectArr(item.subcategory)


    }



    const uploadStudyMaterial = async () => {
        try {

            if (studyMaterialFile != "") {
                let formData = new FormData();
                formData.append("title", studyMaterialTitle);
                formData.append("file", studyMaterialFile);
                // console.log(studyMaterialTitle)
                let res = await uploadStudyMaterialInstructor(formData)
                alert(res.data.message)
                if (res.data.success) {
                    getStudyMaterial()
                    setStudyMaterialModalVisible(false)
                }
            }
            else {
                alert("Please Upload Study Material")
            }

        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err, "error");
                alert(err);
            }
        }
    }



    const getLanguages = async () => {
        try {
            let res = await getAllLanguages()
            if (res.status == 200 || res.status == 304) {


                // console.log(res.data)

                let tempArr = res.data.data.map(el => {
                    let obj = { name: el.name, id: nanoid() }
                    return obj
                })


                setLanguagesArr(tempArr)
                setFilteredLanguageArr(tempArr)



            }
            else {
                alert("Languages Not Found")
            }
        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err);
                alert(err);
            }
        }


    }




    const getCategory = async () => {
        try {
            let res = await getAllCategories()
            // console.log(JSON.stringify(res, null, 2))
            // console.log(res)
            if (res.status == 200 || res.status == 304) {


                let tempArr = res.data.data

                setClassCategoryArr(tempArr)
                setFilteredClassCategoryArr(tempArr)


            }
            else {
                alert("Languages Not Found")
            }
        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err);
                alert(err);
            }
            // console.log(err)
        }
    }

    const getAllCountries = async () => {
        try {
            let res = await getAllCountries()
            if (res) {

                // console.log(res)
            }
        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err);
                alert(err);
            }
        }
    }


    const getStudyMaterial = async () => {
        try {
            let res = await getStudyMaterialsInstructor()
            if (res.status == 200 || res.status == 304) {
                // console.log("qqqqqqqqqqqqqqqqqqqq!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                // console.log(res.data.data, "response Data")
                setItems([...res?.data?.data?.map(el => ({ ...el, name: el.title }))])
            }
        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err);
                alert(err);
            }
        }
    }



    const getInstructor = async () => {
        try {
            let res = await getInstructorData()
            // console.log(res)
            if (res.status == 200 || res.status == 304) {
                // console.log("qqqqqqqqqqqqqqqqqqqq!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                console.log(JSON.stringify(res.data.data.currency, null, 2), "response instructor Data")
                // setItems(res?.data)
                setCurrency(res.data.data.currency)
            }
        }
        catch (err) {
            console.log(err)
            if (err?.response?.data?.message) {
                console.error(err, "err");
                console.error(err?.response?.data?.message, "err");
                alert(err?.response?.data?.message);
            }
            else {
                // console.log(err, "err");
                alert(err);
                console.log("test")
            }
        }
    }











    useEffect(() => {
        getLanguages()
        getCategory()
        getStudyMaterial()
        getInstructor()
    }, [])







    //////////////step check For Validations
    const checkValidationsStep2 = () => {
        if (title != "" && maximumStudents != "" && tagsArr.length > 0 && selectedClassLevel != "" && selectedClassCategory != "" && selectedClassSubject != "" && selectedLanguage != "")
            return false
        else
            return true
    }

    const checkValidationsStep3 = () => {
        let requirementIsNotEmpty = requirementsArr.every(el => el.value != "")
        console.log(requirementsArr, "requirements")
        console.log(requirementIsNotEmpty, "asd")
        let leansArrIsNotEmpty = learnArr.every(el => el.value != "")
        console.log(leansArrIsNotEmpty, "learn")
        if (classDescription != "" && requirementIsNotEmpty == true && leansArrIsNotEmpty == true) {
            return false
        }
        else {
            return true
        }
    }
    const checkValidationsStep4 = () => {
        let slotsDateAreValid = slotsArr.every(el => el.date != "")
        let slotsTimeIsValid = slotsArr.every(el => el.time != "")
        let slotsDurationIsValid = slotsArr.every(el => el.duration != "")
        if (classType == 0) {
            if (slotsDateAreValid && slotsTimeIsValid) {
                return false
            }
            else {
                return true
            }
        }
        else if (classType == 1) {
            if (slotsDurationIsValid && slotsTimeIsValid && slotsDateAreValid) {
                return false
            }
            else {
                return true
            }
        }
    }





    //////////check valid date 
    const checkValidDate = (val) => {
        let currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime()
        if (currentDate < new Date(val).getTime()) {
            setSlotDate(val)
        }
        else {
            alert("this date is already passed")
        }
    }
    const checkValidTime = (val) => {
        let currentTime = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds()).getTime()
        if (currentTime <= new Date(new Date(slotsArr[selectedSlot].date).getFullYear(), new Date(slotsArr[selectedSlot].date).getMonth(), new Date(slotsArr[selectedSlot].date).getDate(), new Date(val).getHours(), new Date(val).getMinutes(), new Date(val).getSeconds()).getTime()) {
            setSlotTime(val)
        }
        else {
            alert("this time is already passed")
        }
    }







    const handleSubmit = async () => {
        try {

            let timeZone = moment.tz.guess();
            console.log(timeZone)

            let temprequirementsArr = requirementsArr.map(el => el.value)

            let tagArr = tagsArr.map(el => el.name)

            let learningArr = learnArr.map(el => el.value)

            let dateArr = slotsArr.map(el => `${new Date(el.date).getDate()}/${new Date(el.date).getMonth()}/${new Date(el.date).getFullYear()}`)

            let timeArr = slotsArr.map(el => el.time)

            let durationArr = slotsArr.map(el => el.durationValue)



            if (classType == 0) {
                if (!finalSelectedItemsArr.length > 0) {
                    alert("Please Select Study materials")
                }
                if (classImage == "") {
                    alert("Please upload class image")
                }
                if (youtubeUrl == "") {
                    alert("Please enter youtube link")
                }
                if (courseVideo == "") {
                    alert("Please upload course video")
                }
                let formdata = new FormData();

                formdata.append("type", "free");
                formdata.append("title", title);
                formdata.append("level", selectedClassLevel);
                formdata.append("category_id", `${selectedsubjectId == "" ? selectedClassCategoryId : selectedsubjectId}`);
                formdata.append("students", maximumStudents);
                tagArr.forEach(val => {
                    formdata.append(`tags[]`, val);
                });
                formdata.append("timezone", timeZone);
                formdata.append("description", classDescription);
                formdata.append("language", selectedLanguage);
                learningArr.forEach(val => {
                    formdata.append(`learn[]`, val);
                });
                temprequirementsArr.forEach(val => {
                    formdata.append(`requirement[]`, val);
                });
                finalSelectedItemsArr.forEach(val => {
                    formdata.append(`material[]`, val);
                });
                formdata.append("image", classImage);
                formdata.append("preview_video", courseVideo);
                formdata.append("video_url", youtubeUrl);
                dateArr.forEach(val => {
                    formdata.append(`date[]`, val);
                });
                timeArr.forEach(val => {
                    formdata.append(`time[]`, val);
                });
                durationArr.forEach(val => {
                    formdata.append(`duration[]`, val);
                });

                let object = {
                    type: "free",
                    title: title,
                    level: selectedClassLevel,
                    category_id: `${selectedsubjectId == "" ? selectedClassCategoryId : selectedsubjectId}`,
                    students: maximumStudents,
                    tags: tagArr,
                    timezone: timeZone,
                    description: classDescription,
                    language: selectedLanguage,
                    time: timeArr,
                    loearning: learningArr,
                    requirements: temprequirementsArr,
                    material: finalSelectedItemsArr,
                    date: dateArr,
                    time: timeArr,
                    duration: durationArr,
                    image: classImage,
                    preview_video: courseVideo,
                    video_url: youtubeUrl,
                }




                console.log(JSON.stringify(object, null, 2), "object ")
                let res = await createCreateClass(formdata)
                if (res.data.success) {
                    alert(res.data.message)
                }
                console.log(JSON.stringify(res, null, 2), "response")
                console.log(JSON.stringify(res.data, null, 2), "response data")


            }
            else {


                let formdata = new FormData();

                formdata.append("type", "paid");
                formdata.append("title", title);
                formdata.append("level", selectedClassLevel);
                formdata.append("category_id", `${selectedsubjectId == "" ? selectedClassCategoryId : selectedsubjectId}`);
                formdata.append("students", maximumStudents);
                tagArr.forEach(val => {
                    formdata.append(`tags[]`, val);
                });
                formdata.append("timezone", timeZone);
                formdata.append("description", classDescription);
                formdata.append("language", selectedLanguage);
                learningArr.forEach(val => {
                    formdata.append(`learn[]`, val);
                });
                temprequirementsArr.forEach(val => {
                    formdata.append(`requirement[]`, val);
                });
                finalSelectedItemsArr.forEach(val => {
                    formdata.append(`material[]`, val);
                });
                formdata.append("image", classImage);
                formdata.append("preview_video", courseVideo);
                formdata.append("video_url", youtubeUrl);
                dateArr.forEach(val => {
                    formdata.append(`date[]`, val);
                });
                timeArr.forEach(val => {
                    formdata.append(`time[]`, val);
                });
                durationArr.forEach(val => {
                    formdata.append(`duration[]`, val);
                });
                formdata.append("price", classFee);
                formdata.append("discount", discount);
                formdata.append("currency", currency);


                console.log(JSON.stringify(formdata, null, 2), "Paid")
                let res = await createCreateClass(formdata)
                if (res.data.success) {
                    alert(res.data.message)
                }
                console.log(res.data, "Res data")
                console.log(res, "res")
            }

        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err, "easdas");
                alert(err);
            }
        }
    }



    return (
        <>
            <Header rootProps={props} name="Create Class" />
            <View style={styles.container}>
                <View style={styles.stepperContainer}>
                    <View style={{ flex: 1 }}>
                        <ProgressSteps activeLabelColor={Colors.lightThemeBg1Color} completedLabelColor={Colors.lightThemeButtonColor} activeStepIconBorderColor={Colors.lightThemeBg1Color} completedStepIconColor={Colors.lightThemeButtonColor} completedProgressBarColor={Colors.lightThemeButtonColor}>

                            <ProgressStep label="Step 1" nextBtnTextStyle={buttonTextStyle} nextBtnStyle={BtnStyle} previousBtnTextStyle={buttonTextStyle}>
                                <View style={{ width: wp(95), alignSelf: "center", alignItems: "center", marginVertical: 30 }}>
                                    <View style={{ width: wp(80), }}>
                                        <Text style={styles.Heading}>
                                            Please Choose a class type
                                        </Text>
                                    </View>

                                    <Pressable onPress={() => setClassType(0)} style={[styles.btn, classType == 0 ? styles.btnActive : styles.btnInactive]}>
                                        <Image source={Images.free} style={styles.btnImage} />
                                        <Text style={styles.btnTxt}>Free Class</Text>
                                    </Pressable>
                                    <Pressable onPress={() => setClassType(1)} style={[styles.btn, classType == 1 ? styles.btnActive : styles.btnInactive]}>
                                        <Image source={Images.paid} style={styles.btnImage} />
                                        <Text style={styles.btnTxt}>Paid Class</Text>
                                    </Pressable>
                                </View>
                            </ProgressStep>



                            <ProgressStep nextBtnDisabled={checkValidationsStep2()} onNext={() => { }} label="Step 2" nextBtnTextStyle={buttonTextStyle} nextBtnStyle={BtnStyle} previousBtnStyle={BtnStyle} previousBtnTextStyle={buttonTextStyle}>
                                <View style={{ alignItems: 'center' }}>
                                    <ScrollView contentContainerStyle={{ width: wp(95), height: "100%", alignSelf: "center", alignItems: "center", marginVertical: 30 }}>
                                        <View style={{ width: wp(80), }}>
                                            <Text style={styles.label}>Title</Text>

                                            <TextInput placeholder="Title" onSubmitEditing={() => setClassLevelModalVisible(true)} onChangeText={(val) => { setTitle(val); }} placeholderTextColor="rgba(0,0,0,0.4)" value={title} style={styles.TextInputStyles} />

                                            <Text style={styles.label}>Class Level</Text>
                                            <Pressable style={styles.modalToggleBtn} onPress={() => setClassLevelModalVisible(true)}>
                                                <Text style={styles.modalToggleBtnTxt} >{selectedClassLevel == "" ? "Class Level" : selectedClassLevel}</Text>
                                                <Icon name="chevron-down" />
                                            </Pressable>
                                            <Text style={styles.label}>In What language will you teach?</Text>
                                            <Pressable style={styles.modalToggleBtn} onPress={() => setLanguageModalVisible(true)}>
                                                <Text style={styles.modalToggleBtnTxt} >{selectedLanguage == "" ? "Select Language" : selectedLanguage}</Text>
                                                <Icon name="chevron-down" />
                                            </Pressable>
                                            <Text style={styles.label}>Maximum Students</Text>

                                            <TextInput placeholder="Maximum Students" ref={maxStudentsRef} keyboardType="number-pad" onChangeText={(val) => { setmaximumStudents(val); }} onSubmitEditing={() => tagsRef.current.focus()} value={maximumStudents} placeholderTextColor="rgba(0,0,0,0.4)" style={styles.TextInputStyles} />

                                            <Text style={styles.label}>Class Category</Text>
                                            <Pressable style={styles.modalToggleBtn} onPress={() => setClassCategoryModalVisible(true)}>
                                                <Text style={styles.modalToggleBtnTxt} >{selectedClassCategory == "" ? "Select Class Category" : selectedClassCategory}</Text>
                                                <Icon name="chevron-down" />
                                            </Pressable>
                                            <Text style={styles.label}>Class Subject</Text>
                                            <Pressable style={styles.modalToggleBtn} onPress={() => setClassSubjectModalVisible(true)}>
                                                <Text style={styles.modalToggleBtnTxt} >{selectedClassSubject == "" ? "Select Class Subject" : selectedClassSubject}</Text>
                                                <Icon name="chevron-down" />
                                            </Pressable>
                                            <Text style={styles.label}>Tags (Maximum 6 tags)</Text>
                                            <TextInput returnKeyType="done" ref={tagsRef} onSubmitEditing={() => { updateTagsArr() }} placeholder="(Maximum 6 tags)" onChangeText={(val) => setTag(val)} placeholderTextColor="rgba(0,0,0,0.4)" style={styles.TextInputStyles} />


                                            {
                                                tagsArr && tagsArr.length > 0 &&
                                                <FlatList
                                                    data={tagsArr}
                                                    numColumns={2}
                                                    renderItem={({ item, index }) => {
                                                        return (
                                                            <View style={styles.tagContainer}>
                                                                <Text style={styles.tagTxt}>{item.name}</Text>
                                                                <Pressable style={styles.tagBtn} onPress={() => deleteTags(index)}>
                                                                    <Text style={styles.tagBtnTxt}>X</Text>
                                                                </Pressable>
                                                            </View>
                                                        )
                                                    }
                                                    }
                                                    keyExtractor={(item, index) => `${index}`}
                                                />
                                            }



                                        </View>
                                    </ScrollView>
                                </View>
                            </ProgressStep>



                            <ProgressStep label="Step 3" nextBtnDisabled={checkValidationsStep3()} onNext={() => { }} nextBtnTextStyle={buttonTextStyle} nextBtnStyle={BtnStyle} previousBtnStyle={BtnStyle} previousBtnTextStyle={buttonTextStyle}>
                                <View style={{ width: wp(95), alignSelf: "center", alignItems: "center", marginVertical: 30 }}>
                                    <View style={{ width: wp(80), }}>
                                        <Text style={styles.label}>Class Description</Text>

                                        <TextInput placeholder="Class Description" multiline={true} onChangeText={(val) => { setClassDescription(val); }} value={classDescription} placeholderTextColor="rgba(0,0,0,0.4)" style={[styles.TextInputStyles, { height: 80 }]} />
                                        <Text style={styles.label}>What you'll learn</Text>

                                        <FlatList
                                            data={learnArr}
                                            scrollEnabled={false}
                                            contentContainerStyle={{ paddingVertical: 10 }}
                                            keyExtractor={(item, index) => item.id}
                                            renderItem={renderLearnFlatlist}
                                        />

                                        <Text style={styles.label}>Requirements</Text>

                                        <FlatList
                                            data={requirementsArr}
                                            scrollEnabled={false}
                                            contentContainerStyle={{ paddingVertical: 10 }}
                                            keyExtractor={(item, index) => item.id}
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <>
                                                        <View style={styles.textInputContainer}>
                                                            <TextInput style={[styles.TextInputStyles, { flex: 0.9 }]} value={item.value} onChangeText={(val) => { updateRequirementTextValue(val, index); }} placeholder="e.g. You will need a copy of... " />
                                                            <Pressable onPress={() => addRequirementArrItem()} style={styles.addBtnContainer}>
                                                                <Icon name="plus" color="green" size={22} />
                                                            </Pressable>
                                                            <Pressable onPress={() => deleteRequirementArrItem(index)} style={styles.DeleteBtnContainer}>
                                                                <Icon name="minus" color="red" size={22} />
                                                            </Pressable>
                                                        </View>
                                                    </>
                                                )
                                            }}
                                        />





                                    </View>
                                </View>
                            </ProgressStep>






                            <ProgressStep label="Step 4" nextBtnDisabled={checkValidationsStep4()} nextBtnTextStyle={buttonTextStyle} nextBtnStyle={BtnStyle} previousBtnStyle={BtnStyle} previousBtnTextStyle={buttonTextStyle}>
                                <View style={{ width: wp(95), alignSelf: "center", alignItems: "center", marginVertical: 30 }}>
                                    <View style={{ width: wp(80), }}>
                                        {
                                            classType == 1 &&
                                            <>
                                                <Text style={styles.label}>Select Currency</Text>
                                                {/* <Text style={[styles.TextInputStyles]} >{currency}</Text> */}
                                                <TextInput placeholder="Select Currency" value={currency} editable={false} onChangeText={(val) => setTitle(val)} placeholderTextColor="rgba(0,0,0,0.4)" style={[styles.TextInputStyles]} />
                                            </>
                                        }

                                        {
                                            classType == 1 &&
                                            <>

                                                <Text style={styles.label}>Class Fee</Text>
                                                <TextInput placeholder="0" ref={feeRef} value={classFee} keyboardType="number-pad" onSubmitEditing={() => discountRef.current.focus()} onChangeText={(val) => setClassFee(val)} placeholderTextColor="rgba(0,0,0,0.4)" style={[styles.TextInputStyles]} />
                                            </>
                                        }

                                        {
                                            classType == 1 &&
                                            <>
                                                <Text style={styles.label}>Discount in (%)</Text>
                                                <TextInput placeholder="e.g. 35" keyboardType="number-pad" value={discount} ref={discountRef} onChangeText={(val) => setDiscount(val)} placeholderTextColor="rgba(0,0,0,0.4)" style={[styles.TextInputStyles]} />
                                            </>
                                        }




                                        <FlatList
                                            data={slotsArr}
                                            keyExtractor={(item, index) => item.id}
                                            ListFooterComponent={
                                                <>
                                                    <View style={[styles.flexRow, { alignItems: "center", justifyContent: "space-around", marginTop: 20 }]}>
                                                        <Pressable onPress={() => addSlots()} style={styles.addBtnContainer}>
                                                            <Icon name="plus" color="green" size={22} />
                                                        </Pressable>
                                                        <Pressable onPress={() => removeSlots()} style={styles.DeleteBtnContainer}>
                                                            <Icon name="minus" color="red" size={22} />
                                                        </Pressable>
                                                    </View>
                                                </>
                                            }
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <>
                                                        <Text style={[styles.Heading, { marginTop: 20 }]}>Slot {index + 1}</Text>
                                                        <View style={styles.flexRow}>
                                                            <View style={[styles.flexColumn, { flex: 1, marginRight: 5 }]}>
                                                                <Text style={[styles.label, { fontSize: 12 }]}>Pick Date</Text>
                                                                <Pressable style={[styles.modalToggleBtn, { width: "100%" }]} onPress={() => { setSelectedSlot(index); setdateModalVisible(true); }}>
                                                                    <Text style={styles.modalToggleBtnTxt} >{item?.date == "" ? "Date" : `${new Date(item?.date).getFullYear()}/${new Date(item?.date).getMonth()}/${new Date(item?.date).getDate()}`}</Text>
                                                                    <Icon name="chevron-down" />
                                                                </Pressable>

                                                            </View>
                                                            <View style={[styles.flexColumn, { flex: 1, marginRight: 5 }]}>
                                                                <Text style={[styles.label, { fontSize: 12 }]}>Pick Time</Text>
                                                                <Pressable style={[styles.modalToggleBtn, { width: "100%" }]} onPress={() => { setSelectedSlot(index); setTimeModalVisible(true); }}>
                                                                    <Text style={styles.modalToggleBtnTxt} >{item?.time == "" ? "Time" : item?.time}</Text>
                                                                    <Icon name="chevron-down" />
                                                                </Pressable>
                                                            </View>
                                                            {
                                                                classType == 1 &&
                                                                <View style={[styles.flexColumn, { flex: 1, }]}>
                                                                    <Text style={[styles.label, { fontSize: 12 }]}>Course Duration</Text>
                                                                    <Pressable style={[styles.modalToggleBtn, { width: "100%" }]} onPress={() => { setSelectedCourseDuration(index); setDurationModalVisible(true) }}>
                                                                        <Text style={styles.modalToggleBtnTxt} >{item?.duration == "" ? "Duration" : item?.duration}</Text>
                                                                        <Icon name="chevron-down" />
                                                                    </Pressable>
                                                                </View>
                                                            }
                                                        </View>
                                                    </>
                                                )
                                            }}
                                        />
                                    </View>
                                </View>
                            </ProgressStep>



                            <ProgressStep label="Step 5" onSubmit={() => handleSubmit()} nextBtnTextStyle={buttonTextStyle} nextBtnStyle={BtnStyle} previousBtnStyle={BtnStyle} previousBtnTextStyle={buttonTextStyle}>
                                <View style={{ width: wp(95), alignSelf: "center", alignItems: "center", marginVertical: 30 }}>
                                    <ScrollView style={{ width: wp(80), }}>

                                        <Text style={styles.label}>Have any study material to share with students?</Text>

                                        <Pressable style={styles.modalToggleBtn} onPress={() => setStudyMaterialModalVisible(true)}>
                                            <Text style={styles.modalToggleBtnTxt} >{studyMaterialFile == "" ? "Upload Study" : "Study Material Uploaded"}</Text>
                                            <Icon name="image" />
                                        </Pressable>


                                        <Text style={styles.label}>Study Material</Text>


                                        <MultiSelect
                                            hideTags
                                            items={items}
                                            uniqueKey="id"
                                            ref={multiselect}
                                            onSelectedItemsChange={(val) => { onSelectedItemsChange(val); setSelectedItemsArrValues(val) }}
                                            selectedItems={selectedItems}
                                            selectText="Pick Items"
                                            searchInputPlaceholderText="Search Items..."
                                            altFontFamily="ProximaNova-Light"
                                            tagRemoveIconColor="#CCC"
                                            tagBorderColor="#CCC"
                                            tagTextColor="#CCC"
                                            selectedItemTextColor="#CCC"
                                            selectedItemIconColor="#CCC"
                                            itemTextColor="#000"
                                            displayKey="name"
                                            submitButtonColor="#CCC"
                                            submitButtonText="Submit"
                                            styleMainWrapper={{ borderRadius: 20, padding: 5, backgroundColor: "#fff" }}
                                        />

                                        {
                                            finalSelectedItemsArr.length > 0 &&
                                            <Text style={styles.label}> Selected Study Materials List</Text>
                                        }

                                        <FlatList
                                            contentContainerStyle={{ width: wp(90), }}
                                            data={finalSelectedItemsArr}
                                            scrollEnabled={false}
                                            // numColumns=/{4}
                                            keyExtractor={(item, index) => item.id}
                                            renderItem={({ item, index }) => {
                                                return (
                                                    <Text>
                                                        {index + 1}. {item}
                                                        {/* {index < finalSelectedItemsArr.length - 1 ? ", " : " "} */}
                                                    </Text>

                                                )

                                            }}
                                        />



                                        <Text style={[styles.label, { textAlign: "left", alignSelf: "flex-start" }]}>Upload Class Image</Text>
                                        <Pressable style={[styles.TextInputStyles, { width: "100%", display: "flex", justifyContent: "center", alignItems: "center", marginVertical: 5, paddingVertical: 5 }]} onPress={() => pickClassImage()}>
                                            {
                                                classImage != "" ?
                                                    <Text style={{ color: "black" }}>{classImage.uri}</Text>
                                                    :
                                                    <>
                                                        {/* <Text style={{ color: "black" }}>{studyMaterialFile.uri}</Text> */}
                                                        <Icon name="image" size={25} color="black" />
                                                    </>
                                            }
                                        </Pressable>




                                        <Text style={styles.label}>Enter Youtube Link</Text>
                                        <TextInput onChangeText={(val) => { setYoutubeUrl(val) }} placeholder="Enter Youtube Link here" placeholderTextColor="rgba(0,0,0,0.4)" style={styles.TextInputStyles} />




                                        <Text style={[styles.label, { textAlign: "left", alignSelf: "flex-start" }]}>Add an introductory video of yours</Text>
                                        <Text style={styles.textStyle}>
                                            This is your chance to showcase your style of teaching, the topic of your class and any relevant information about your class which you will like students to see in the class description. You can either upload your video on Youtube and add a link here for students to see or upload your video directly on Yocolab.</Text>




                                        <Text style={[styles.label, { textAlign: "left", alignSelf: "flex-start" }]}>Upload Course Video</Text>
                                        <Pressable style={[styles.TextInputStyles, { width: "100%", display: "flex", justifyContent: "center", alignItems: "center", marginVertical: 5, paddingVertical: 5 }]} onPress={() => pickCoursePreview()}>
                                            {
                                                courseVideo != "" ?
                                                    <Text style={{ color: "black" }}>{courseVideo.uri}</Text>
                                                    :
                                                    <>
                                                        {/* <Text style={{ color: "black" }}>{studyMaterialFile.uri}</Text> */}
                                                        <Icon name="image" size={25} color="black" />
                                                    </>
                                            }
                                        </Pressable>

                                    </ScrollView>
                                </View>
                            </ProgressStep>

                        </ProgressSteps>
                    </View>








                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={classLevelModalVisible}
                        onRequestClose={() => {
                            setClassLevelModalVisible(!classLevelModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <FlatList
                                    data={classLevelArr}
                                    ListHeaderComponent={
                                        <>
                                            <Pressable
                                                style={styles.closeModalBtn}
                                                onPress={() => setClassLevelModalVisible(!classLevelModalVisible)}
                                            >
                                                <Text style={styles.textStyle}>X</Text>
                                            </Pressable>
                                            <Text style={styles.modalHeading}>Select Class Level</Text>
                                        </>
                                    }
                                    keyExtractor={({ item, index }) => index}
                                    renderItem={({ item, index }) => {
                                        return (

                                            <>
                                                <Pressable onPress={() => { setSelectedClassLevel(item.name); setClassLevelModalVisible(false); setLanguageModalVisible(true) }}>
                                                    <Text style={styles.ModalTxt}>{item.name}</Text>
                                                </Pressable>

                                            </>
                                        )
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={languageModalVisible}
                        onRequestClose={() => {
                            setLanguageModalVisible(!languageModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <FlatList
                                    data={filteredLanguageArr}
                                    ListHeaderComponent={
                                        <>
                                            <Pressable
                                                style={styles.closeModalBtn}
                                                onPress={() => setLanguageModalVisible(!languageModalVisible)}
                                            >
                                                <Text style={styles.textStyle}>X</Text>
                                            </Pressable>
                                            <Text style={styles.modalHeading}>Select a Language</Text>

                                            <Searchbar
                                                style={{ width: wp(70), marginBottom: 20 }}

                                                placeholder="Search"
                                                onChangeText={(val) => onChangeSearchLanguage(val)}
                                                value={searchQuery}
                                            />
                                        </>
                                    }
                                    keyExtractor={(item, index) => `${item.id}`}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <>
                                                <Pressable onPress={() => { setSelectedLanguage(item.name); setLanguageModalVisible(false); setClassCategoryModalVisible(true) }}>
                                                    <Text style={styles.ModalTxt}>{item.name}</Text>
                                                </Pressable>
                                            </>
                                        )
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={classCategoryModalVisible}
                        onRequestClose={() => {
                            setClassCategoryModalVisible(!classCategoryModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <FlatList
                                    data={filteredClassCategoryArr}
                                    ListHeaderComponent={
                                        <>
                                            <Pressable
                                                style={styles.closeModalBtn}
                                                onPress={() => setClassCategoryModalVisible(!classCategoryModalVisible)}
                                            >
                                                <Text style={styles.textStyle}>X</Text>
                                            </Pressable>

                                            <Text style={styles.modalHeading}>Select a Class Category</Text>

                                            <Searchbar
                                                style={{ width: wp(70), marginBottom: 20 }}
                                                placeholder="Search"
                                                onChangeText={(val) => onChangeSearchClassCategory(val)}
                                                value={searchQuery}
                                            />
                                        </>
                                    }
                                    keyExtractor={({ item, index }) => index}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <>
                                                <Pressable onPress={() => { setSelectedClassCategoryId(item.id); setClassSubjectCategoryArrByClass(item); setClassCategoryModalVisible(false); setClassSubjectModalVisible(true) }}>
                                                    <Text style={styles.ModalTxt}>{item.name}</Text>
                                                </Pressable>
                                            </>
                                        )
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={classSubjectModalVisible}
                        onRequestClose={() => {
                            setClassSubjectModalVisible(!classSubjectModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <FlatList
                                    data={filteredClassSubjectArr}
                                    ListHeaderComponent={
                                        <>
                                            <Pressable
                                                style={styles.closeModalBtn}
                                                onPress={() => setClassSubjectModalVisible(!classSubjectModalVisible)}
                                            >
                                                <Text style={styles.textStyle}>X</Text>
                                            </Pressable>

                                            <Text style={styles.modalHeading}>Select a Class Subject</Text>

                                            <Searchbar
                                                style={{ width: wp(70), marginBottom: 20 }}
                                                placeholder="Search"
                                                onChangeText={(val) => onChangeSearchClassSubject(val)}
                                                value={searchQuery}
                                            />
                                        </>
                                    }
                                    keyExtractor={({ item, index }) => index}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <>
                                                <Pressable onPress={() => { setSelectedsubjectId(item.id); setSelectedClassSubject(item.name); setClassSubjectModalVisible(false); maxStudentsRef.current.focus() }}>
                                                    <Text style={styles.ModalTxt}>{item.name}</Text>
                                                </Pressable>
                                            </>
                                        )
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={durationModalVisible}
                        onRequestClose={() => {
                            setDurationModalVisible(!durationModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <FlatList
                                    data={durationArr}
                                    ListHeaderComponent={
                                        <>
                                            <Pressable
                                                style={styles.closeModalBtn}
                                                onPress={() => setDurationModalVisible(!durationModalVisible)}
                                            >
                                                <Text style={styles.textStyle}>X</Text>
                                            </Pressable>
                                            <Text style={styles.modalHeading}>Select a Duration in Hours</Text>
                                        </>
                                    }
                                    keyExtractor={({ item, index }) => index}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <>
                                                <Pressable onPress={() => { setDurationOfSlot(item.duration, item.value); setDurationModalVisible(false); }}>
                                                    <Text style={styles.ModalTxt}>{item.duration}</Text>
                                                </Pressable>
                                            </>
                                        )
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={dateModalVisible}
                        onRequestClose={() => {
                            setdateModalVisible(!dateModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={[styles.modalView, { alignItems: "center" }]}>
                                <Pressable
                                    style={styles.closeModalBtn}
                                    onPress={() => setdateModalVisible(!dateModalVisible)}
                                >
                                    <Text style={styles.textStyle}>X</Text>
                                </Pressable>
                                <Text style={styles.modalHeading}>Select a Date</Text>
                                <DatePicker
                                    style={[styles.modalToggleBtn, { width: "90%" }]}
                                    date={date}
                                    mode="date"
                                    is24hourSource="locale"
                                    onDateChange={(val) => checkValidDate(val)}
                                />
                                <Pressable onPress={() => { setdateModalVisible(false); }} style={[styles.btn, { backgroundColor: Colors.lightThemeButtonColor, width: "90%" }]}>
                                    <Text style={styles.modalSubmitBtnTxt}>Submit</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={timeModalVisible}
                        onRequestClose={() => {
                            setTimeModalVisible(!timeModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={[styles.modalView, { alignItems: "center" }]}>
                                <Pressable
                                    style={styles.closeModalBtn}
                                    onPress={() => setTimeModalVisible(!timeModalVisible)}
                                >
                                    <Text style={styles.textStyle}>X</Text>
                                </Pressable>
                                <Text style={styles.modalHeading}>Select a Time</Text>
                                <DatePicker
                                    style={[styles.modalToggleBtn, { width: "90%" }]}
                                    date={date}
                                    mode="time"
                                    onDateChange={(val) => checkValidTime(val)}
                                />
                                <Pressable onPress={() => { setTimeModalVisible(false); }} style={[styles.btn, { backgroundColor: Colors.lightThemeButtonColor, width: "90%" }]}>
                                    <Text style={styles.modalSubmitBtnTxt}>Submit</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={studyMaterialModalVisible}
                        onRequestClose={() => {
                            setStudyMaterialModalVisible(!studyMaterialModalVisible);
                        }}
                    >

                        <View style={styles.centeredView}>
                            <View style={[styles.modalView, { paddingHorizontal: "5%" }]}>
                                <Pressable
                                    style={styles.closeModalBtn}
                                    onPress={() => setStudyMaterialModalVisible(!studyMaterialModalVisible)}
                                >
                                    <Text style={styles.textStyle}>X</Text>
                                </Pressable>
                                <Text style={styles.modalHeading}>Study Material</Text>

                                <Text style={[styles.label, { textAlign: "left", alignSelf: "flex-start" }]}>Title</Text>
                                <TextInput placeholder="Title" onChangeText={(val) => setStudyMaterialTitle(val)} placeholderTextColor="rgba(0,0,0,0.4)" style={[styles.TextInputStyles, { width: "100%" }]} />

                                <Text style={[styles.label, { textAlign: "left", alignSelf: "flex-start" }]}>Upload Study File</Text>
                                <Pressable onPress={() => pickImageStudyMaterial()}>
                                    {
                                        studyMaterialFile != "" ?
                                            <Text style={{ color: "black" }}>{studyMaterialFile.uri}</Text>
                                            :
                                            <>
                                                {/* <Text style={{ color: "black" }}>{studyMaterialFile.uri}</Text> */}
                                                <Icon name="image" size={25} color="black" />
                                            </>
                                    }
                                </Pressable>

                                <Pressable onPress={() => uploadStudyMaterial()} style={[styles.btn, { backgroundColor: Colors.lightThemeButtonColor, width: "100%" }]}>
                                    <Text style={styles.modalSubmitBtnTxt}>Submit</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>


                </View>
            </View>
        </>
    )
}


const styles = StyleSheet.create({
    //////containers
    container: {
        display: "flex",
        height: hp(100) - 100,
        width: wp(100),
    },
    stepperContainer: {
        width: wp(95),
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
        paddingTop: 10
    },
    tagContainer: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: Colors.lightThemeButtonColor,
        flex: 1,
        marginVertical: 10,
        borderRadius: 5,
        padding: 10,
        marginHorizontal: wp(1.45),
        justifyContent: "space-between",
        color: "rgba(0,0,0,0.4)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
    },
    textInputContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: 7
    },
























    /////textInput 
    TextInputStyles: {
        width: wp(80),
        borderRadius: 10,
        paddingHorizontal: 15,
        fontSize: 12,
        shadowColor: ColorObj.lightThemeBg1Color,
        color: "rgba(0,0,0,0.4)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        fontFamily: 'Montserrat-Regular',
        backgroundColor: ColorObj.lightThemeBgColor,
    },


























    ///////// txt
    btnTxt: {
        fontFamily: "Montserrat-Medium",
    },
    ModalTxt: {
        color: "black",
        height: 30,
        width: wp(70),
        marginTop: 10
    },
    textStyle: {
        fontFamily: "Montserrat-Medium",
        fontSize: 15,
        color: "rgba(0,0,0,0.5)"
    },
    modalHeading: {
        fontFamily: "Montserrat-Medium",
        color: "black",
        textAlign: "center",
        marginTop: 10,
        marginBottom: 20,
        fontSize: 20

    },
    modalSubmitBtnTxt: {
        fontFamily: "Montserrat-Medium",
        color: "white"
    },
    modalToggleBtnTxt: {
        color: "rgba(0,0,0,0.4)",
        fontSize: 12,
    },

    tagTxt: {
        fontFamily: "Montserrat-SemiBold",
        color: "white",
        flex: 0.8
    },
    tagBtnTxt: {
        fontFamily: "Montserrat-SemiBold",
        color: "white",
        // flex: 0.8
    },
    Heading: {
        fontFamily: "Montserrat-SemiBold",
        fontSize: 20
    },
    label: {
        fontFamily: "Montserrat-Medium",
        fontSize: 15,
        marginTop: 25,
        marginBottom: 10,
        color: "black"
    },














    ////// btn
    btnImage: {
        height: 40,
        width: 40, marginRight: 15
    },
    tagBtn: {
        backgroundColor: "rgba(255,255,255,0.2)",
        height: 20,
        width: 20,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20
    },
    btn: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        width: wp(80),
        marginTop: 45,
        paddingVertical: 10,
        borderRadius: 10
    },
    btnInactive: {
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0.4)",
    },
    btnActive: {
        borderWidth: 2,
        borderColor: Colors.lightThemeBg1Color,
    },
    modalToggleBtn: {
        width: wp(80),
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 16,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        shadowColor: ColorObj.lightThemeBg1Color,

        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        fontFamily: 'Montserrat-Regular',
        backgroundColor: ColorObj.lightThemeBgColor,
    },
    closeModalBtn: {
        width: wp(5),
        marginVertical: 10,
        alignSelf: "flex-end",
    },
    addBtnContainer: {
        borderWidth: 2,
        borderColor: "green",
        height: 40,
        width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20
    },
    DeleteBtnContainer: {
        borderWidth: 2,
        borderColor: "red",
        height: 40,
        width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20
    },



















    //////modals
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0,0,0,0.8)"
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 5,
        width: wp(80),
        minHeight: hp(70),
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },








    /////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column",
    },


})