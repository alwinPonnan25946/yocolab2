import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Pressable, Modal, TextInput, ScrollView, FlatList } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome";
import ColorObj from "../Globals/Colors"
import Header from "../Components/Header";
import { Checkbox, Searchbar } from 'react-native-paper';
import DocumentPicker from 'react-native-document-picker';
import { useRef } from 'react/cjs/react.development';
import { useIsFocused } from '@react-navigation/native';
import { getAllCountries } from '../Services/Country';
import { getAllLanguages } from '../Services/Languages';
import { becomeInstructor } from '../Services/Instructor';

export default function BecomeAnInstructor(props) {
    const isFocused = useIsFocused()


    //////checkbox 
    const [termsChecked, setTermsChecked] = useState(false);
    const [paymentPolicyChecked, setPaymentPolicyChecked] = useState(false);

    //////modals
    const [experienceModalVisible, setExperienceModalVisible] = useState(false);
    const [selectCountryModal, setSelectCountryModal] = useState(false);
    const [selectlanguageModal, setSelectlanguageModal] = useState(false);


    //////search querry
    const [searchQuery, setSearchQuery] = useState('');

    ///////modal Data
    const [experienceArr, setExperienceArr] = useState([
        { index: 1, name: "1+ Years" },
        { index: 2, name: "2+ Years" },
        { index: 3, name: "3+ Years" },
        { index: 4, name: "4+ Years" },
        { index: 5, name: "5+ Years" },
        { index: 6, name: "6+ Years" },
        { index: 7, name: "7+ Years" },
        { index: 8, name: "8+ Years" },
        { index: 9, name: "9+ Years" },
        { index: 10, name: "10+ Years" },
        { index: 11, name: "11+ Years" },
        { index: 12, name: "12+ Years" },
        { index: 13, name: "13+ Years" },
        { index: 14, name: "14+ Years" },
        { index: 15, name: "15+ Years" },
        { index: 16, name: "16+ Years" },
        { index: 17, name: "17+ Years" },
        { index: 18, name: "18+ Years" },
        { index: 19, name: "19+ Years" },
        { index: 20, name: "20+ Years" },
    ]);


    const [countryArr, setCountryArr] = useState([]);
    const [filteredCountryArr, setFilteredCountryArr] = useState([]);



    const [languageArr, setLanguageArr] = useState([
        { index: 1, name: "English" },
        { index: 2, name: "Hindi" },
        { index: 4, name: "French" },
        { index: 5, name: "Spanish" },
    ]);


    const [filteredLanguageArr, setFilteredLanguageArr] = useState([
        { index: 1, name: "English" },
        { index: 2, name: "Hindi" },
        { index: 4, name: "French" },
        { index: 5, name: "Spanish" },
    ]);



    ////////selected Data
    const [selectedExperience, setSelectedExperience] = useState("");
    const [selectedCountry, setSelectedCountry] = useState("");
    const [selectedLanguage, setSelectedLanguage] = useState("");


    //////////these will be submited
    const [profileImage, setProfileImage] = useState("");
    const [expertise, setExpertise] = useState("");
    const [qualification, setQualification] = useState("");
    const [about, setAbout] = useState("");
    const [countryValue, setCountryValue] = useState("");


    ////////search country
    const onChangeSearch = query => {
        setFilteredCountryArr(countryArr.filter(el => el.name.toLowerCase().includes(query.toLowerCase())))
        setSearchQuery(query)
    };
    ////////search Language
    const onChangeSearchLanguage = query => {
        setFilteredLanguageArr(languageArr.filter(el => el.name.toLowerCase().includes(query.toLowerCase())))
        setSearchQuery(query)
    };



    /////////////refs 
    const expertiseRef = useRef()
    const qualificationRef = useRef()
    const aboutRef = useRef()


    /////onSubmit 
    const handleSubmit = async () => {
        try {
            if (expertise == "") {
                alert("Please enter your expertise")
            }
            if (qualification == "") {
                alert("Please enter your qualification")
            }
            if (selectedExperience == "") {
                alert("Please select your experience")
            }
            if (selectedCountry == "") {
                alert("Please select your country")
            }
            if (selectedLanguage == "") {
                alert("Please select your Language")
            }
            if (about == "") {
                alert("Please enter your about info ")
            }
            if (profileImage == "") {
                alert("Please select a profile image ")
            }
            if (paymentPolicyChecked == false) {
                alert("Please read and agree to our payment policy to move forward")
            }
            if (termsChecked == false) {
                alert("Please read and agree to our terms and conditions to move forward")
            }
            else {
                let formData = new FormData()
                formData.append("expert", expertise);
                formData.append("qualification", qualification);
                formData.append("experience", selectedExperience);
                formData.append("country", countryValue);
                formData.append("language", selectedLanguage);
                formData.append("about", about);
                formData.append("image", profileImage);
                let res = await becomeInstructor(formData)
                console.log(res)
                if (res.status == 200 || res.status == 304) {
                    alert(res.data.message);
                    props.navigation.navigate("HomePage");
                }
            }
        }
        catch (err) {
            if (err?.response?.data?.message) {
                console.log(err?.response?.data?.message);
                alert(err?.response?.data?.message);
            }
            else {
                console.log(err);
                alert(err);
            }
        }
    }





    const pickImage = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            setProfileImage(res)
            console.log(
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }






    ////////getting data on init

    const getcountry = async () => {
        try {
            let { data: res, status: statusCode } = await getAllCountries()
            console.log(res.data)
            if (statusCode == 200 || statusCode == 304) {
                setCountryArr(res.data)
                setFilteredCountryArr(res.data)
            }
        }
        catch (err) { console.log(err) }
    }
    const getLanguagesfromApi = async () => {
        try {
            let { data: res, status: statusCode } = await getAllLanguages()
            console.log(res.data)
            if (statusCode == 200 || statusCode == 304) {
                setLanguageArr(res.data)
                setFilteredLanguageArr(res.data)
            }
        }
        catch (err) { console.log(err) }
    }


    const getOnInitData = async () => {
        getcountry()
        getLanguagesfromApi()
    }
    useEffect(() => {
        if (isFocused) {

            getOnInitData()

        }
    }, [isFocused])


    return (
        <>
            <Header rootProps={props} name="Become an Instructor" />
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.headingContainer}>
                    <Text style={styles.Heading}>Fill this form to become an </Text>
                    <Text style={styles.Heading}> Instuctor !!</Text>
                </View>
                <View style={styles.flexColumn}>

                    <Text style={styles.label}>Your Expertise</Text>
                    <TextInput ref={expertiseRef} onSubmitEditing={() => qualificationRef.current.focus()} returnKeyType="next" placeholder="What is my Expertise (Yoga Trainer Etc.)" onChangeText={(val) => setExpertise(val)} placeholderTextColor="rgba(0,0,0,0.2)" style={styles.TextInputStyles} />

                    <Text style={styles.label}>Your Qualification</Text>
                    <TextInput ref={qualificationRef} placeholder="What is my Qualification " onSubmitEditing={() => setExperienceModalVisible(true)} returnKeyType="next" onChangeText={(val) => setQualification(val)} placeholderTextColor="rgba(0,0,0,0.2)" style={styles.TextInputStyles} />

                    <Text style={styles.label}>Your Experience In Years</Text>
                    <Pressable style={styles.modalToggleBtn} onPress={() => setExperienceModalVisible(true)}>
                        <Text style={styles.modalToggleBtnTxt} >{selectedExperience == "" ? "Your Experience In Years" : selectedExperience}</Text>
                        <Icon name="chevron-down" />
                    </Pressable>

                    <Text style={styles.label}>Select Your Country</Text>
                    <Pressable style={styles.modalToggleBtn} onPress={() => setSelectCountryModal(true)}>
                        <Text style={styles.modalToggleBtnTxt} >{selectedCountry == "" ? "Select Your Country" : selectedCountry}</Text>
                        <Icon name="chevron-down" />
                    </Pressable>

                    <Text style={styles.label}>Language</Text>
                    <Pressable style={styles.modalToggleBtn} onPress={() => setSelectlanguageModal(true)}>
                        <Text style={styles.modalToggleBtnTxt} >{selectedLanguage == "" ? "Select Your Language" : selectedLanguage}</Text>
                        <Icon name="chevron-down" />
                    </Pressable>

                    <Text style={styles.label}>About Me </Text>
                    <TextInput onChangeText={(val) => setAbout(val)} ref={aboutRef} placeholder="Something about yourself" placeholderTextColor="rgba(0,0,0,0.2)" multiline={true} style={[styles.TextInputStyles, { minHeight: 80 }]} />

                    <Text style={styles.label}>Upload Profile Picture</Text>
                    <Pressable style={styles.modalToggleBtn} onPress={() => pickImage()}>
                        {
                            profileImage ?

                                <Text style={styles.modalToggleBtnTxt} >{profileImage.name}</Text>
                                :
                                <Text style={styles.modalToggleBtnTxt} >Pick a Profile Picture</Text>
                        }
                        <Icon name="image" />
                    </Pressable>

                    <View style={[styles.flexRow, { alignItems: "center", width: wp(80), justifyContent: "flex-start" }]}>
                        <Pressable onPress={() => setTermsChecked(!termsChecked)}>
                            <Checkbox.Item color={ColorObj.darkThemeButtonColor} status={termsChecked ? "checked" : "unchecked"} />
                        </Pressable>
                        <Text style={styles.CheckboxTxt}>Accept</Text>
                        <Pressable style={{ marginLeft: 5 }}>
                            <Text style={{ color: "#5C68CA" }}>terms and condition</Text>
                        </Pressable>
                    </View>


                    <View style={[styles.flexRow, { alignItems: "center", width: wp(80), justifyContent: "flex-start" }]}>
                        <Pressable onPress={() => setPaymentPolicyChecked(!paymentPolicyChecked)}>
                            <Checkbox.Item color={ColorObj.darkThemeButtonColor} status={paymentPolicyChecked ? "checked" : "unchecked"} />
                        </Pressable>
                        <Text style={styles.CheckboxTxt}></Text>
                        <Text style={styles.CheckboxTxt}>Accept</Text>
                        <Pressable style={{ marginLeft: 5 }}>
                            <Text style={{ color: "#5C68CA" }}>payment policy</Text>
                        </Pressable>
                    </View>
                </View>

                <Pressable style={styles.btn} onPress={() => handleSubmit()}>
                    <Text style={styles.btnTxt}>
                        Register
                    </Text>
                </Pressable>









                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={experienceModalVisible}
                    onRequestClose={() => {
                        setExperienceModalVisible(!experienceModalVisible);
                    }}
                >

                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>



                            <FlatList
                                data={experienceArr}
                                ListHeaderComponent={
                                    <>
                                        <Pressable
                                            style={[styles.closeModalBtn]}
                                            onPress={() => setExperienceModalVisible(!experienceModalVisible)}
                                        >
                                            <Text style={styles.textStyle}>X</Text>
                                        </Pressable>
                                        <Text style={styles.modalHeading}>Select your Experience</Text>
                                    </>
                                }
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => {
                                    return (
                                        <>
                                            <Pressable onPress={() => { setSelectedExperience(item.name); setSelectCountryModal(true); setExperienceModalVisible(false) }}>
                                                <Text style={styles.ModalTxt}>{item.name}</Text>
                                            </Pressable>
                                        </>
                                    )
                                }}
                            />





                        </View>
                    </View>

                </Modal>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={selectCountryModal}
                    onRequestClose={() => {
                        setSelectCountryModal(!selectCountryModal);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>


                            <FlatList
                                data={filteredCountryArr}
                                ListHeaderComponent={
                                    <>
                                        <Pressable
                                            style={[styles.closeModalBtn]}
                                            onPress={() => { setSelectCountryModal(!selectCountryModal) }}
                                        >
                                            <Text style={styles.textStyle}>X</Text>
                                        </Pressable>
                                        <Text style={styles.modalHeading}>Select your Country</Text>

                                        <Searchbar
                                            style={{ width: wp(70), marginBottom: 20 }}
                                            placeholder="Search"
                                            onChangeText={onChangeSearch}
                                            value={searchQuery}
                                        />
                                    </>
                                }
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => {
                                    return (

                                        <>
                                            <Pressable onPress={() => { setCountryValue(item.value); setSelectedCountry(item.name); setSelectlanguageModal(true); setSelectCountryModal(false) }}>
                                                <Text style={styles.ModalTxt}>{item.name}</Text>
                                            </Pressable>
                                        </>
                                    )
                                }}
                            />


                        </View>
                    </View>
                </Modal>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={selectlanguageModal}
                    onRequestClose={() => {
                        setSelectlanguageModal(!selectlanguageModal);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>

                            <FlatList
                                data={filteredLanguageArr}
                                ListHeaderComponent={
                                    <>
                                        <Pressable
                                            style={styles.closeModalBtn}
                                            onPress={() => setSelectlanguageModal(!selectlanguageModal)}
                                        >
                                            <Text style={styles.textStyle}>X</Text>
                                        </Pressable>
                                        <Text style={styles.modalHeading}>Select your Language</Text>

                                        <Searchbar
                                            style={{ width: wp(70), marginBottom: 20 }}

                                            placeholder="Search"
                                            onChangeText={onChangeSearchLanguage}
                                            value={searchQuery}
                                        />
                                    </>
                                }
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => {
                                    return (

                                        <>
                                            <Pressable onPress={() => { aboutRef.current.focus(); setSelectedLanguage(item.name); setSelectlanguageModal(false) }}>
                                                <Text style={styles.ModalTxt}>{item.name}</Text>
                                            </Pressable>

                                        </>
                                    )
                                }}
                            />





                        </View>
                    </View>
                </Modal>


            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorObj.lightThemeGreyBgColor,
        paddingBottom: 80
    },
    headingContainer: {
        backgroundColor: ColorObj.lightThemeBg1Color,
        height: wp(40),
        width: wp(100),
        display: "flex",
        justifyContent: "center",
        borderBottomRightRadius: 60,
        paddingHorizontal: wp(10),
    },


    ModalTxt: {
        color: "black",
        height: 30,
        width: wp(70),
        marginTop: 10
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0,0,0,0.8)"
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 5,
        width: wp(80),
        minHeight: hp(70),
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },


    closeModalBtn: {
        width: wp(5),
        marginVertical: 10,
        alignSelf: "flex-end",
    },
    textStyle: {
        fontFamily: "Montserrat-Medium",
        fontSize: 15,
        color: "rgba(0,0,0,0.5)"
    },
    modalHeading: {
        fontFamily: "Montserrat-Medium",
        color: "black",
        textAlign: "center",
        marginTop: 10,
        marginBottom: 20,
        fontSize: 20

    },

    Heading: {
        fontFamily: "Montserrat-Medium",
        color: ColorObj.lightThemeBgColor,
        fontSize: 20
    },
    CheckboxTxt: {
        color: "rgba(0,0,0,0.4)"
    },
    flexRow: {
        display: "flex",
        flexDirection: "row",
    },

    flexColumn: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center", marginTop: hp(5)
    },

    label: {
        fontFamily: "Montserrat-Medium",
        marginVertical: 10,
        textAlign: "left",
        width: wp(80),
        paddingLeft: 10,
        marginTop: 25,
        color: "rgba(0,0,0,0.3)"
    },
    modalToggleBtn: {
        width: wp(80),
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 15,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        shadowColor: ColorObj.lightThemeBg1Color,

        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        fontFamily: 'Montserrat-Regular',
        backgroundColor: ColorObj.lightThemeBgColor,
    },

    modalToggleBtnTxt: {
        color: "rgba(0,0,0,0.2)",
        fontSize: 12,
    },
    TextInputStyles: {
        width: wp(80),
        borderRadius: 10,
        paddingHorizontal: 15,
        fontSize: 12,
        shadowColor: ColorObj.lightThemeBg1Color,
        color: "rgba(0,0,0,0.2)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        fontFamily: 'Montserrat-Regular',
        backgroundColor: ColorObj.lightThemeBgColor,
    },
    btn: {
        width: wp(80),
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    forgotBtn: {
        width: wp(80),
        backgroundColor: "transparent",
        height: 45,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(3),
    },
    forgotBtnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeTextColor
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    socialMediaBtn: {
        backgroundColor: ColorObj.lightThemeBgColor,
        borderRadius: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        height: 40, width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    AccentColoredTxt: {
        fontFamily: "Montserrat-Regular",
        color: ColorObj.lightThemeBg1Color
    },
    getStartedBtnTxt: {
        fontFamily: "Montserrat-Bold",
        color: ColorObj.lightThemeBg1Color
    },
})