import React from 'react'
import { View, Text, StyleSheet, ActivityIndicator, Image } from 'react-native'
import ColorObj from '../Globals/Colors'
import Images from '../Globals/Images'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Svg, { Circle } from "react-native-svg"
export default function Loading(props) {
    return (
        <View style={styles.mainContainer}>
            <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
            {/* <Image source={require('../../assets/img/preloader.gif')} /> */}
            {/* <Svg
                xmlns="http://www.w3.org/2000/svg"
                style={{
                    margin: "auto",
                    background: "#fff",
                }}
                width={200}
                height={200}
                viewBox="0 0 100 100"
                preserveAspectRatio="xMidYMid"
                // display="block"
                {...props}
            >
                <Circle
                    cx={50}
                    cy={50}
                    r={0}
                    fill="none"
                    stroke="#e90c59"
                    strokeWidth={2}
                ></Circle>
                <Circle
                    cx={50}
                    cy={50}
                    r={0}
                    fill="none"
                    stroke="#46dff0"
                    strokeWidth={2}
                ></Circle>
            </Svg> */}
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        display: 'flex',
        position: 'absolute',
        height: hp(100),
        width: wp(100),
        top: 0,
        flexDirection: 'column',
        alignItems: 'center', justifyContent: 'center'
    },

})