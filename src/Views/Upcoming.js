import React, { useState, useEffect, useContext } from 'react'
import { View, Text, StyleSheet, ScrollView, Pressable, Image, FlatList } from 'react-native'
import Header from "../Components/Header";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';
import { useIsFocused } from '@react-navigation/native';
import { getTopCourses, happeningTodayCourses, myCourses } from '../Services/Courses';
import { getAllCategories } from '../Services/Categories';
import ColorObj from "../Globals/Colors";
import { nanoid } from 'nanoid/non-secure'
import { Searchbar } from 'react-native-paper';
import { Chip } from 'react-native-paper';


import moment from 'moment';
import 'moment-timezone';

import { loadingContext } from '../../App'
export default function Upcoming(props) {

    const [coursesArr, setCoursesArr] = useState([]);
    const focused = useIsFocused()

    const [todayCoursesArr, setTodayCoursesArr] = useState([]);
    const [weekCoursesArr, setWeekCoursesArr] = useState([]);
    const [isLoading, setIsLoading] = useContext(loadingContext);

    const getCourses = async () => {
        try {
            setIsLoading(true)
            const { data: res } = await myCourses();
            if (res.success) {
                let currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                let tempArr = [...res.data.courses.map(el => ({ ...el, uniqueId: nanoid() }))]

                let todayTempArr = tempArr.filter(el => new Date(el.date).getTime() == currentDate.getTime())

                let weekTempArr = tempArr.filter(el => moment().isoWeek() == moment(el.date).isoWeek())
                console.log("@@@@@@@@@@")
                console.log(JSON.stringify(todayTempArr, null, 2), JSON.stringify(weekTempArr, null, 2))
                setCoursesArr(tempArr)
                setTodayCoursesArr(todayTempArr)
                setWeekCoursesArr(weekTempArr)
            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.error(error)
        }
    }

    const handleOninit = () => {
        getCourses()
    }


    const renderTodayCoursesArr = ({ item, index }) => {
        return (
            <View style={styles.todayCourseCardContainer}>
                <View style={styles.todayCardImageContainer}>
                    <Image source={{ uri: item?.image }} style={styles.todayCardImage} />
                </View>
                <View style={styles.todayCardTextContainer}>
                    <Text style={styles.todayCardHeading}>{item.title}</Text>
                    <Text style={styles.todayCardText}>By : {item.teacher_name}</Text>
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between', flexWrap: 'wrap', marginVertical: 20, width: '80%' }]}>
                        <Text style={[styles.todayCardText, { fontSize: 12 }]}>{item.date},{item.time}</Text>
                        <Text style={[styles.todayCardText, { fontSize: 12 }]}>{item.price}</Text>


                    </View>
                </View>
            </View>
        )
    }

    useEffect(() => {
        if (focused)
            handleOninit()
    }, [focused])

    return (
        <>
            <Header rootProps={props} name="Upcoming" />
            <View contentContainerStyle={styles.container}>
                <View style={{ marginHorizontal: 10 }}>

                    <FlatList
                        data={todayCoursesArr}
                        keyExtractor={(item, index) => `${item.uniqueId}`}
                        ListHeaderComponent={
                            <View style={[styles.flexRow, { justifyContent: "space-between", width: "100%", alignSelf: "center", marginVertical: hp(2) }]}>
                                <Text style={styles.SectionHeading}>Today</Text>
                            </View>
                        }
                        ListEmptyComponent={
                            <Text>No Courses Found</Text>
                        }
                        renderItem={renderTodayCoursesArr}
                        ListFooterComponent={
                            <FlatList
                                data={weekCoursesArr}
                                keyExtractor={(item, index) => `${item.uniqueId}`}
                                scrollEnabled={false}
                                ListHeaderComponent={
                                    <View style={[styles.flexRow, { justifyContent: "space-between", width: "100%", alignSelf: "center", marginVertical: hp(2) }]}>
                                        <Text style={styles.SectionHeading}>This Week</Text>
                                    </View>
                                }
                                ListEmptyComponent={
                                    <Text>No Courses Found</Text>
                                }
                                renderItem={renderTodayCoursesArr}
                                
                            />
                        }
                    />
                </View>
            </View>

        </>
    )
}

const styles = StyleSheet.create({
    //////container
    container: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#FBFBFB",
        paddingBottom: 60,
    },
    dataContainer: {
        width: wp(100),
        alignSelf: "center",
    },
    categoryDataContainer: {
        position: "absolute",
        bottom: 10,
        backgroundColor: "rgba(0,0,0,0.5)",
        paddingHorizontal: 5,
        paddingVertical: 5,
        borderRadius: 5,
        left: 10,
    },
    seeAllBtnTxt: {
        fontSize: 14,
        color: "grey",
        fontFamily: "Montserrat-Medium",
    },
    SectionHeading: {
        fontSize: 16,
        fontFamily: "Montserrat-Bold",
    },


    //////btn
    seeAllBtn: {
        backgroundColor: "transparent"
    },
    //////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column"
    },


    // card
    todayCourseCardContainer: {
        width: wp(100),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        display: 'flex',
        flexDirection: 'row'
    },
    todayCardImageContainer: {
        padding: 10,
        width: '30%',
    },
    todayCardTextContainer: {
        width: '70%',
        paddingVertical: 10
    },
    todayCardImage: {
        height: 100,
        width: '100%'
    },
    todayCardHeading: {
        fontSize: 18,
        fontFamily: 'Montserrat-SemiBold',
        marginVertical: 2
    },
    todayCardText: {
        fontSize: 14,
        fontFamily: 'Montserrat-Regular'
    }
})