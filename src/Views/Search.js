import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, ScrollView, Pressable, Image, FlatList, ActivityIndicator } from 'react-native'
import Header from "../Components/Header";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SliderBox } from "react-native-image-slider-box";
import Icon from 'react-native-vector-icons/FontAwesome';
import { useIsFocused } from '@react-navigation/native';
import { getTopCourses, happeningTodayCourses } from '../Services/Courses';
import { getTopInstructors } from '../Services/Instructor';
import { getAllCategories } from '../Services/Categories';
import ColorObj from "../Globals/Colors";
import { nanoid } from 'nanoid/non-secure'
import { Searchbar } from 'react-native-paper';
import { Chip } from 'react-native-paper';

export default function Search(props) {

    const [topCategoryArr, setTopCategoryArr] = useState([]);

    const [searchQuery, setSearchQuery] = useState('');

    const [happeningArr, setHappeningArr] = useState([]);
    const focused = useIsFocused()

    const onChangeSearch = query => setSearchQuery(query);


    const getCategories = async () => {
        try {
            let { data: CategoriesRes, } = await getAllCategories()
            if (CategoriesRes) {

                let CategoriesArrTemp = CategoriesRes.data.map(el => ({ ...el, uniqueId: nanoid() })).filter(el => el.top == 1)

                setTopCategoryArr(CategoriesArrTemp)
                console.log(JSON.stringify(CategoriesArrTemp, null, 2), "topCategoriesRes")
                // console.log(CategoriesArrTemp, "topCategoriesRes")
            }
        } catch (error) {
            console.error(error)
        }
    }

    const getHappeningToday = async () => {
        try {
            let { data: happeningtodayRes } = await happeningTodayCourses()
            if (happeningtodayRes) {
                setHappeningArr(happeningtodayRes.data.map(el => ({ ...el, uniqueId: nanoid() })))
            }
        } catch (error) {
            console.error(error)
        }
    }


    const handleOninit = () => {
        getCategories()
        getHappeningToday()
    }


    const renderHappeningToday = ({ item, index }) => {
        return (

            <Pressable style={styles.coursesCard}>
                <Image source={{ uri: item.image }} style={styles.coursesCardImage} />
                <View style={[styles.flexColumn, { flex: 1, marginVertical: 15 }]}>
                    <View style={[styles.flexColumn, { alignSelf: "center", width: "78%" }]}>
                        <Text style={styles.courseName}>{item.title}</Text>
                        <Text style={styles.courseDescription}>by {item.instructor}</Text>
                        <Text style={styles.courseDescription}>{item.date}</Text>
                    </View>
                    <View style={{ alignItems: "center", display: "flex", flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
                        <Text style={styles.coursePrice}>{item.price}</Text>
                        <View style={[styles.flexRow, { alignItems: "center", justifyContent: "center", marginBottom: 6 }]}>
                            <Icon name="star" size={15} color={"#fcba05"} />
                            <Text> ({item.rating}) </Text>
                        </View>
                    </View>
                </View>
            </Pressable>

        )
    }

    useEffect(() => {
        if (focused)
            handleOninit()
    }, [focused])

    return (
        <>
            <Header rootProps={props} name="Search" />
            <View contentContainerStyle={styles.container}>
                <View style={{ marginTop: 20, marginBottom: 15, paddingHorizontal: 20 }}>

                    <Searchbar
                        style={{ fontFamily: 'Montserrat-Regular' }}
                        placeholder="Search"
                        onChangeText={onChangeSearch}
                        value={searchQuery}
                    />
                </View>
                <View style={{ paddingHorizontal: 15, flexWrap: 'wrap', flexDirection: 'row' }} >
                    {topCategoryArr.map((el => {
                        return (
                            <Chip key={el.uniqueId} style={{ margin: 5 }} textStyle={{ fontSize: 14, fontFamily: 'Montserrat-Regular' }} mode="outlined" onPress={() => console.log('Pressed')}>{el.name}</Chip>
                        )
                    }))}

                </View>
                <FlatList
                    data={happeningArr}
                    // contentContainerStyle={{ paddingBottom: 100 }}
                    numColumns={2}
                    scrollEnabled={true}

                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                        <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
                    }
                    ListHeaderComponent={
                        
                            <View style={[styles.flexRow, { justifyContent: "space-between", width: "90%", alignSelf: "center", marginVertical: hp(2) }]}>
                                <Text style={styles.SectionHeading}>Happening Today</Text>
                                <Pressable style={styles.seeAllBtn} onPress={() => { console.log("asda") }}>
                                    <Text style={styles.seeAllBtnTxt}>See All</Text>
                                </Pressable>
                            </View>
                        
                    }
                    keyExtractor={(item, index) => `${item.uniqueId}`}
                    renderItem={renderHappeningToday}
                />

            </View>
        </>
    )
}

const styles = StyleSheet.create({
    //////container
    container: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#FBFBFB",
        paddingBottom: 60
    },
    dataContainer: {
        width: wp(100),
        alignSelf: "center",
    },
    categoryDataContainer: {
        position: "absolute",
        bottom: 10,
        backgroundColor: "rgba(0,0,0,0.5)",
        paddingHorizontal: 5,
        paddingVertical: 5,
        borderRadius: 5,
        left: 10,
    },

    ///////card
    coursesCard: {
        width: "45%",
        // padding: 10,
        marginHorizontal: "2.5%",
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    CategoryCard: {
        width: wp(41),
        height: wp(41),
        // backgroundColor:'red',
        borderRadius: 15,
        display: "flex",
        position: "relative",
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    InstructorCard: {
        width: wp(89),
        borderRadius: 15,
        display: "flex",
        alignSelf: "center",
        padding: 25,
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },



    /////////txt
    coursePrice: {
        fontFamily: "Montserrat-Bold",
    },
   
    courseName: {
        fontFamily: "Montserrat-Bold",
        // width: "50%"
        fontSize: 13,
        marginBottom: 5
    },
    
    courseDescription: {
        fontFamily: "Montserrat-Medium",
        color: "grey",
        fontSize: 11,
        marginTop: 3
    },
    seeAllBtnTxt: {
        fontSize: 14,
        color: "grey",
        fontFamily: "Montserrat-Medium",
    },
    SectionHeading: {
        fontSize: 16,
        fontFamily: "Montserrat-Bold",
    },


    //////btn
    seeAllBtn: {
        backgroundColor: "transparent"
    },


    //////images
    coursesCardImage: {
        height: 125,
        // width: 60,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    categoryCardImage: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
        borderRadius: 15
    },
 

    //////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column"
    },
})