import React, { useState, useEffect, useContext } from 'react'
import { View, Text, StyleSheet, ScrollView, Pressable, Image, FlatList } from 'react-native'
import Header from "../Components/Header";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Ionicons';
import { useIsFocused } from '@react-navigation/native';
import ColorObj from "../Globals/Colors";
import { nanoid } from 'nanoid/non-secure'
import { Chip } from 'react-native-paper';


import moment from 'moment';
import 'moment-timezone';

import { loadingContext } from '../../App'
import { getSpecificClassBySlug } from '../Services/Courses';
import Images from '../Globals/Images';

import CountDown from 'react-native-countdown-component';
import Colors from '../Globals/Colors';
import { addClassToWishlist } from '../Services/user';

export default function SpecificClass(props) {


    const focused = useIsFocused()

    const [specificClassObj, setSpecificClassObj] = useState({});

    const [isLoading, setIsLoading] = useContext(loadingContext);
    const getSpecificClass = async () => {
        try {
            setIsLoading(true)
            let tempClassObj = props.route.params.courseObj
            console.log(JSON.stringify(tempClassObj, null, 2))
            const { data: res } = await getSpecificClassBySlug(tempClassObj?.slug)
            if (res.success) {
                console.log(JSON.stringify(res.data, null, 2))
                let tempObj = res.data;
                console.log(new Date(`${tempObj.date} ${tempObj.time}`).toDateString())
                if (new Date(`${tempObj.date} ${tempObj.time}`).getTime() < new Date().getTime()) {
                    tempObj.timePassed = true
                }
                else {
                    tempObj.timePassed = false;
                    let classTime = new Date(`${tempObj.date} ${tempObj.time}`).getTime();
                    let currentTime = new Date().getTime();
                    console.log(tempObj.date, tempObj.time)
                    console.log(currentTime, classTime)
                    tempObj.timer = currentTime - classTime
                    console.log(tempObj.timer)
                }
                console.log(tempObj)
                setSpecificClassObj(tempObj)

            }
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
            console.error(error)
        }
    }


    const addToWishList = async () => {
        try {

            const { data: res } = await addClassToWishlist(specificClassObj?.id)
            console.log()
            if (res) {
                alert(res.message)
            }

        } catch (error) {
            console.error(error)
            alert(error.message)
        }
    }


    const handleOninit = async () => {
        getSpecificClass()
    }

    useEffect(() => {
        if (focused)
            handleOninit()
    }, [focused])



    return (
        <>

            <Header rootProps={props} name="Course" />
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.innerContainer}>
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between' }]}>
                        <Text style={styles.mainHeading}>{specificClassObj?.title}</Text>
                        <Icon name={specificClassObj?.is_wishlist ? "heart" : "heart-outline"} color="black" size={20} onPress={() => addToWishList()} />
                    </View>
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between' }]}>
                        <Pressable onPress={() => props.navigation.navigate('SpecificTeacher', { slug: specificClassObj?.teacher?.slug })}>

                            <Text style={styles.mainSubHeading}>By : {specificClassObj?.teacher?.name}</Text>
                        </Pressable>
                        <Text style={styles.mainSubHeading}>Joined By</Text>
                    </View>
                    <View>
                        {specificClassObj?.image ?
                            <Image source={{ uri: specificClassObj?.image }} style={{ borderRadius: 10, height: 200, width: '100%', marginTop: 10, resizeMode: "cover" }} />
                            :
                            <Image source={Images.intro1} style={{ height: 200, width: '100%', marginTop: 10, resizeMode: "cover" }} />
                        }
                    </View>
                    {!specificClassObj?.timePassed ?
                        <></>
                        :

                        <CountDown
                            until={10000}
                            onFinish={() => alert('class Started')}
                            // onPress={() => alert('hello')}
                            size={20}
                            running={!isLoading}
                            digitStyle={{ backgroundColor: '#000', marginHorizontal: 20, marginTop: 20 }}
                            digitTxtStyle={{ color: Colors.lightThemeBgColor, fontFamily: 'Montserrat-Regular', fontSize: 20 }}
                            timeLabelStyle={{ color: '#000', fontFamily: 'Montserrat-Regular', fontSize: 14 }}
                            showSeparator={true}

                        />
                    }
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }]}>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Date :</Text>  {specificClassObj?.date}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Time :</Text> {specificClassObj?.time}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Duration :</Text> {specificClassObj?.duration}</Text>

                    </View>
                    <View style={[styles.flexRow, { alignItems: 'center', justifyContent: 'space-between', marginTop: 10 }]}>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Language :</Text> {specificClassObj?.language}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >Students :</Text> {specificClassObj?.students}</Text>
                        <Text style={styles.underImageText}><Text style={{ fontFamily: 'Montserrat-SemiBold' }} >size : </Text>{specificClassObj?.class_size}</Text>

                    </View>
                    <View>
                        <Text style={[styles.mainHeading, { marginTop: 20 }]}>{specificClassObj?.price}</Text>
                    </View>
                    <View>
                        <Pressable style={styles.btn} onPress={()=>props.navigation.navigate('zoom')}>
                            <Text style={styles.btnTxt}>Buy Now</Text>
                        </Pressable>
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.overviewHeading}>Overview</Text>
                        <Text style={[styles.overviewSubheading, { marginTop: 10 }]}>Course Description</Text>

                        <Text style={styles.overviewText}>{specificClassObj?.desciption}</Text>

                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.overviewSubheading}>What you'll learn</Text>
                        {specificClassObj?.learn?.map((el, i) => {
                            return (
                                <Text key={i} style={styles.overviewText}>{i + 1}. {el}</Text>
                            )
                        })}

                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.overviewSubheading}>Requirements</Text>
                        {specificClassObj?.requirement?.map((el, i) => {
                            return (
                                <Text key={i} style={styles.overviewText}>{i + 1}. {el}</Text>
                            )
                        })}

                    </View>
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    //////container
    container: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#FBFBFB",
        paddingBottom: 60,
    },
    innerContainer: {
        width: wp(90),
        marginHorizontal: 20,
        marginTop: 10
        // backgroundColor:'red',
        // height:hp(90)
    },
    mainHeading: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 24,
    },
    mainSubHeading: {
        marginVertical: 10,
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
    }
    ,  //////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column"
    },
    underImageText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14
    },
    btn: {
        width: '100%',
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    overviewHeading: {
        fontSize: 22,
        fontFamily: 'Montserrat-Bold',
        textDecorationLine: 'underline'
    },
    overviewSubheading: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        marginVertical: 5
    },
    overviewText: {
        fontFamily: 'Montserrat-Regular'
    }
})