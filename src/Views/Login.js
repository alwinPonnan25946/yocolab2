import React, { useState, useEffect, useContext, useRef } from 'react';
import { View, Text, Image, StyleSheet, Pressable, ImageBackground, TextInput, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome";
import ColorObj from "../Globals/Colors";
import { authContext } from '../Navigation/Stack/RootStack';
import { loadingContext } from '../../App';
import { getUserRoleData, setJwt, setRoleToStorage, userLogin } from '../Services/user';

export default function Login(props) {
    const [isAuthorized, setIsAuthorized] = useContext(authContext);
    const [isLoading, setIsLoading] = useContext(loadingContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');



    const emailRef = useRef();
    const passwordRef = useRef();

    const getUserRole = async () => {
        try {
            let res = await getUserRoleData()
            if (res.status == 200 || res.status == 304) {
                await setRoleToStorage(res.data.role)
                setIsAuthorized(true)

            }
        }
        catch (err) {
            console.error(err)
        }
    }



    const handleSubmit = async () => {
        try {
            setIsLoading(true)
            let formData = new FormData();
            formData.append('email', email)
            formData.append('password', password)
            const res = await userLogin(formData);
            if (res.status == 200 || res.status == 304) {
                await setJwt(res.data.token)
                getUserRole()
                alert("Logged In")
            }
            setIsLoading(false)




        } catch (error) {
            setIsLoading(false)
            console.error(error.response.data)
            alert(error.response.data.message)
        }
    }

    return (
        <ScrollView style={styles.container}>
            <View style={styles.headingContainer}>
                <Image style={styles.logoImage} resizeMode="contain" resizeMethod="resize" source={require("../../assets/img/Yocolab-03.png")} />
                <View>
                    <Text style={styles.Heading}>Welcome </Text>
                    <Text style={styles.Heading}> Back !!</Text>
                </View>
            </View>
            <View style={styles.flexColumn}>
                <Text style={styles.label}>Email</Text>
                <TextInput placeholder="Email" ref={emailRef} onSubmitEditing={() => passwordRef.current.focus()} onChangeText={(val) => setEmail(val)} placeholderTextColor="rgba(0,0,0,0.2)" style={styles.TextInputStyles} />
                <Text style={styles.label}>Password</Text>
                <TextInput placeholder="Password" ref={passwordRef} placeholderTextColor="rgba(0,0,0,0.2)" onChangeText={(val) => setPassword(val)} secureTextEntry={true} style={styles.TextInputStyles} />
            </View>
            <Pressable style={styles.btn} onPress={() => handleSubmit()}>
                <Text style={styles.btnTxt}>
                    Log In
                </Text>
            </Pressable>


            <Pressable style={styles.forgotBtn} onPress={() => props.navigation.navigate("ForgotPassword")}>
                <Text style={styles.forgotBtnTxt}>
                    Forgot Password?
                </Text>
            </Pressable>






            <View style={[styles.flexRow, { width: wp(80), justifyContent: "center", alignSelf: "center" }]}>
                <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#5155DB" name="facebook" /></Pressable>
                {/* <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#F987AC" name="twitter" /></Pressable> */}
                <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#708FEB" name="google" /></Pressable>
            </View>

            <View style={[styles.flexRow, { width: wp(80), justifyContent: "center", alignSelf: "center", marginTop: hp(5) }]}>

                <Text style={styles.AccentColoredTxt}>Don't have an account ? </Text>

                <Pressable onPress={() => props.navigation.navigate("Register")}>
                    <Text style={styles.getStartedBtnTxt}>Get Started</Text>
                </Pressable>
            </View>

        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorObj.lightThemeGreyBgColor,
        flex: 1
    },
    logoImage: {
        height: 70,
        width: 70,
        marginRight: 20
    },
    headingContainer: {
        backgroundColor: ColorObj.lightThemeBg1Color,
        height: wp(40),
        width: wp(100),
        paddingTop: hp(3),
        display: "flex",
        flexDirection: "row",
        borderBottomRightRadius: 60,
        paddingHorizontal: wp(10),
    },
    Heading: {
        fontFamily: "Montserrat-Medium",
        color: ColorObj.lightThemeBgColor,
        fontSize: 28
    },

    flexRow: {
        display: "flex",
        flexDirection: "row",
    },

    flexColumn: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center", marginTop: hp(5)
    },

    label: {
        fontFamily: "Montserrat-Medium",
        marginVertical: 10,
        textAlign: "left",
        width: wp(80),
        paddingLeft: 10,
        marginTop: 25,
        color: "rgba(0,0,0,0.3)"
    },

    TextInputStyles: {
        width: wp(80),
        borderRadius: 10,
        paddingHorizontal: 15,
        shadowColor: ColorObj.lightThemeBg1Color,
        color: "rgba(0,0,0,0.6)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        fontFamily: 'Montserrat-Regular',
        backgroundColor: ColorObj.lightThemeBgColor,
    },
    btn: {
        width: wp(80),
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    forgotBtn: {
        width: wp(80),
        backgroundColor: "transparent",
        height: 45,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(3),
    },
    forgotBtnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeTextColor
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    socialMediaBtn: {
        backgroundColor: ColorObj.lightThemeBgColor,
        borderRadius: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        height: 40, width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    AccentColoredTxt: {
        fontFamily: "Montserrat-Regular",
        color: ColorObj.lightThemeBg1Color
    },
    getStartedBtnTxt: {
        fontFamily: "Montserrat-Bold",
        color: ColorObj.lightThemeBg1Color
    },
})