import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Pressable, ScrollView, Image, ActivityIndicator } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome";
import ColorObj from "../Globals/Colors"
import Header from "../Components/Header";
import { useIsFocused } from '@react-navigation/native';
import { getUserRole } from '../Services/user';
import { getInstructorData } from '../Services/Instructor';
import { getStudentData } from '../Services/Student';




export default function Profile(props) {
    const isFocused = useIsFocused()
    const [teacherProfileData, setTeacherProfileData] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [date, setDate] = useState("");
    const [studentData, setStudentData] = useState("");
    const getProfileData = async () => {
        try {
            let role = await getUserRole()
            if (role && role == "teacher") {
                let res = await getInstructorData()
                if (res.status == 304 || res.status == 200) {
                    console.log(JSON.stringify(res.data.data, null, 2), "response")
                    let date = res.data.data.date.split(" ")[0]
                    setDate(date)
                    setTeacherProfileData(res.data.data)
                    setIsLoading(false)
                }
            }
            else {
                let res = await getStudentData()
                if (res.status == 304 || res.status == 200) {
                    console.log(JSON.stringify(res.data.data, null, 2), "Student response")
                    setStudentData(res.data.data)
                    setIsLoading(false)
                }
            }
        }
        catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        if (isFocused) {
            getProfileData()
        }

    }, [isFocused])



    return (
        <>
            <Header rootProps={props} name="Profile" />

            {
                isLoading ?
                    <View style={{ display: "flex", flex: 1, justifyContent: "center", alignItems: "center", }}>
                        <ActivityIndicator size={"large"} color="#0000ff" />
                    </View>
                    :
                    <ScrollView contentContainerStyle={styles.container}>

                        <View style={{ backgroundColor: ColorObj.lightThemeButtonColor, paddingTop: 30 }}>

                            <View style={styles.flexRowJustifyCenter}>
                                <View style={styles.profileImageContainer}>
                                    <Image source={{ uri: teacherProfileData.image }} style={styles.profileImage} />
                                </View>
                                {/* <Pressable style={styles.btn}>
                                <Text style={styles.btnTxt}>
                                Follow
                                </Text>
                            </Pressable> */}
                            </View>
                            <Text style={styles.userName}>{teacherProfileData.name}</Text>
                        </View>

                        <View style={[styles.profileDetailsContainer, styles.flexRow]}>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{teacherProfileData.students}</Text>
                                <Text style={styles.detailHeading}>Students</Text>
                            </View>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{teacherProfileData.rating}</Text>
                                <Text style={styles.detailHeading}>Rating</Text>
                            </View>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{teacherProfileData.country}</Text>
                                <Text style={styles.detailHeading}>Country</Text>
                            </View>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{teacherProfileData.total_courses}</Text>
                                <Text style={styles.detailHeading}>Classes</Text>
                            </View>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{date}</Text>
                                <Text style={styles.detailHeading}>Since</Text>
                            </View>
                            <View style={[styles.flexColumn, styles.detailsSectionContainer]}>
                                <Text style={styles.detailTxt}>{teacherProfileData.language}</Text>
                                <Text style={styles.detailHeading}>Language</Text>
                            </View>
                        </View>

                        <View style={[styles.detailsCard, { marginTop: 50 }]}>
                            <Text style={styles.pageHeading}>
                                Expertise
                            </Text>
                            <Text style={styles.pageSmallTxt}>
                                {teacherProfileData.expert}
                            </Text>
                        </View>

                        <View style={styles.detailsCard}>
                            <Text style={styles.pageHeading}>
                                Qualification
                            </Text>
                            <Text style={styles.pageSmallTxt}>
                                {teacherProfileData.qualification}
                            </Text>
                        </View>
                        <View style={styles.detailsCard}>
                            <Text style={styles.pageHeading}>
                                Experience
                            </Text>
                            <Text style={styles.pageSmallTxt}>
                                {teacherProfileData.experience}
                            </Text>

                        </View>

                        <View style={styles.detailsCard}>
                            <Text style={styles.pageHeading}>
                                About
                            </Text>
                            <Text style={styles.pageSmallTxt}>
                                {teacherProfileData.about}
                            </Text>
                        </View>

                    </ScrollView>
            }

        </>
    )
}

const styles = StyleSheet.create({
    /////////////containers
    container: {
        backgroundColor: ColorObj.lightThemeGreyBgColor,
        paddingBottom: 80
    },
    profileDetailsContainer: {
        width: wp(90),
        alignSelf: "center",
        flexWrap: "wrap",
        backgroundColor: "white",
        marginTop: 20,
        paddingVertical: 10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    detailsSectionContainer: {
        width: "33%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
    },

    /////////txts
    btnTxt: {
        fontFamily: "Montserrat-Bold",
        color: "white"
    },
    userName: {
        fontFamily: "Montserrat-SemiBold",
        color: "white",
        fontSize: 25,
        textAlign: "center",
        marginVertical: 10
    },
    detailHeading: {
        fontFamily: "Montserrat-Regular",
        fontSize: 13,
        marginTop: 5,
    },
    detailTxt: {
        fontFamily: "Montserrat-SemiBold",
        fontSize: 18,
    },

    pageHeading: {
        fontSize: 20,
        fontFamily: "Montserrat-Bold",
        alignSelf: "center",
        color: "black",
    },
    pageSmallTxt: {
        fontSize: 13,
        fontFamily: "Montserrat-Regular",
        alignSelf: "center",
        marginTop: 10,
        color: "rgba(0,0,0,0.7)",
    },

    //////cards
    detailsCard: {
        width: wp(90),
        padding: 15,
        backgroundColor: ColorObj.lightThemeBgColor,
        marginTop: 30,
        borderRadius: 10,
        alignSelf: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },





    //////Image Container
    profileImageContainer: {
        backgroundColor: ColorObj.lightThemeBg1Color,
        borderRadius: 120,
        padding: 2,
    },




    /////images
    profileImage: {
        height: 120,
        width: 120,
        borderRadius: 120,
    },








    ///////btn
    btn: {
        backgroundColor: ColorObj.lightThemeButtonColor,
        position: "absolute",
        right: wp(3),
        height: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 15,
        borderRadius: 7
    },



















    ///////flex Classes
    flexRow: {
        display: 'flex',
        flexDirection: 'row',
    },
    flexColumn: {
        display: 'flex',
        flexDirection: 'column',
    },
    flexRowJustifyCenter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    flexRowJustifyBetween: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    flexRowJustifyAround: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    flexColumnJustifyCenter: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    flexColumnJustifyBetween: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    flexColumnJustifyAround: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
})







