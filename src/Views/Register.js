import React, { useState, useEffect } from 'react'
import { View, Text, Image, StyleSheet, Pressable, ImageBackground, TextInput, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome";
import { useRef } from 'react/cjs/react.development';
import ColorObj from "../Globals/Colors"
import { userRegister } from '../Services/user';

export default function Register(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');



    //////refs
    const emailRef = useRef();
    const passwordRef = useRef();
    const confirmPasswordRef = useRef();
    const fnameRef = useRef();
    const lnameRef = useRef();



    const handleSubmit = async () => {
        try {
            if (email != "") {
                alert("Please enter an email")
            }
            if (password != "") {
                alert("Please enter your password")
            }
            if (confirmPassword != "") {
                alert("Please enter your password again in confirm password input")
            }
            if (fname != "") {
                alert("Please enter your first name")
            }
            if (lname != "") {
                alert("Please enter your last name")
            }
            else {
                let formData = new FormData();
                formData.append('email', email);
                formData.append('password', password);
                formData.append('cpassword', confirmPassword);
                formData.append('fname', fname);
                formData.append('lname', lname);
                const res = await userRegister(formData);
                if (res.status == 200 || res.status == 304) {
                    alert(res.data.message);
                }
            }
        } catch (error) {
            console.error(error)
        }
    }




    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.headingContainer}>
                <Image style={styles.logoImage} resizeMode="contain" resizeMethod="resize" source={require("../../assets/img/Yocolab-03.png")} />
                <View>
                    <Text style={styles.Heading}>Register to Join </Text>
                    <Text style={styles.Heading}> Yocolab</Text>
                </View>
            </View>
            <View style={styles.flexColumn}>

                <View style={[styles.flexRow, { justifyContent: "space-between", width: wp(90) }]}>
                    <View style={{ width: wp(42.5) }}>
                        <Text style={styles.label}>First name</Text>
                        <TextInput onSubmitEditing={() => lnameRef.current.focus()} onChangeText={(val) => setFname(val)} placeholder="First Name" placeholderTextColor="rgba(0,0,0,0.2)" style={[styles.TextInputStyles, { width: "100%" }]} />
                    </View>
                    <View style={{ width: wp(42.5) }}>
                        <Text style={styles.label}>Last name</Text>
                        <TextInput placeholder="Last Name" ref={lnameRef} onSubmitEditing={() => emailRef.current.focus()} onChangeText={(val) => setLname(val)} placeholderTextColor="rgba(0,0,0,0.2)" style={[styles.TextInputStyles, { width: "100%" }]} />
                    </View>
                </View>
                <Text style={styles.label}>Email</Text>
                <TextInput placeholder="Email" ref={emailRef} onSubmitEditing={() => passwordRef.current.focus()} onChangeText={(val) => setEmail(val)} placeholderTextColor="rgba(0,0,0,0.2)" style={styles.TextInputStyles} />


                <Text style={styles.label}>Password</Text>
                <TextInput placeholder="Password" ref={passwordRef} onSubmitEditing={() => confirmPasswordRef.current.focus()} onChangeText={(val) => setPassword(val)} placeholderTextColor="rgba(0,0,0,0.2)" secureTextEntry={true} style={styles.TextInputStyles} />

                <Text style={styles.label}>Confirm Password</Text>
                <TextInput placeholder="Confirm Password" ref={confirmPasswordRef} onChangeText={(val) => setConfirmPassword(val)} placeholderTextColor="rgba(0,0,0,0.2)" secureTextEntry={true} style={styles.TextInputStyles} />
            </View>

            <Pressable style={styles.btn} onPress={() => handleSubmit()}>
                <Text style={styles.btnTxt}>
                    Sign Up
                </Text>
            </Pressable>


            <View style={[styles.flexRow, { width: wp(80), justifyContent: "center", alignSelf: "center", marginTop: hp(2) }]}>
                <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#5155DB" name="facebook" /></Pressable>
                {/* <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#F987AC" name="twitter" /></Pressable> */}
                <Pressable style={styles.socialMediaBtn}><Icon size={25} color="#708FEB" name="google" /></Pressable>
            </View>

            <View style={[styles.flexRow, { width: wp(80), justifyContent: "center", alignSelf: "center", marginTop: hp(5) }]}>

                <Text style={styles.AccentColoredTxt}>Already have an account ? </Text>

                <Pressable onPress={() => props.navigation.navigate("Login")}>
                    <Text style={styles.getStartedBtnTxt}>Login</Text>
                </Pressable>
            </View>

        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorObj.lightThemeGreyBgColor,
        paddingBottom: 20
    },
    logoImage: {
        height: 70,
        width: 70,
        marginRight: 20
    },
    headingContainer: {
        backgroundColor: ColorObj.lightThemeBg1Color,
        height: wp(40),
        width: wp(100),
        paddingTop: hp(3),
        display: "flex",
        flexDirection: "row",
        borderBottomRightRadius: 60,
        paddingHorizontal: wp(10),
    },
    Heading: {
        fontFamily: "Montserrat-Medium",
        color: ColorObj.lightThemeBgColor,
        fontSize: 28
    },

    flexRow: {
        display: "flex",
        flexDirection: "row",
    },

    flexColumn: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: hp(3)
    },

    label: {
        fontFamily: "Montserrat-Medium",
        marginVertical: 10,
        textAlign: "left",
        width: wp(90),
        paddingLeft: 10,
        marginTop: 25,
        color: "rgba(0,0,0,0.3)"
    },

    TextInputStyles: {
        color: "rgba(0,0,0,0.2)",
        width: wp(90),
        borderRadius: 10,
        paddingHorizontal: 10,
        shadowColor: ColorObj.lightThemeBg1Color,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        backgroundColor: ColorObj.lightThemeBgColor,
    },
    btn: {
        width: wp(90),
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    forgotBtn: {
        width: wp(90),
        backgroundColor: "transparent",
        height: 45,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(3),
    },
    forgotBtnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeTextColor
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    socialMediaBtn: {
        backgroundColor: ColorObj.lightThemeBgColor,
        borderRadius: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        height: 40, width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    AccentColoredTxt: {
        fontFamily: "Montserrat-Regular",
        color: ColorObj.lightThemeBg1Color
    },
    getStartedBtnTxt: {
        fontFamily: "Montserrat-Bold",
        color: ColorObj.lightThemeBg1Color
    },
})