
import React, { useState, useEffect,useContext } from 'react'
import { View, Text, StyleSheet, ScrollView, Pressable, Image, FlatList, ActivityIndicator } from 'react-native'
import Header from "../Components/Header";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SliderBox } from "react-native-image-slider-box";
import Icon from 'react-native-vector-icons/FontAwesome';
import { useIsFocused } from '@react-navigation/native';
import { getTopCourses, happeningTodayCourses } from '../Services/Courses';
import { getTopInstructors } from '../Services/Instructor';
import { getAllCategories } from '../Services/Categories';
import ColorObj from "../Globals/Colors";
import { nanoid } from 'nanoid/non-secure'
import { loadingContext } from '../../App';
export default function Home(props) {


    const isFocused = useIsFocused()


    const [isLoading, setIsLoading] = useContext(loadingContext);
    const [images, setImages] = useState([
        "https://source.unsplash.com/1024x768/?nature",
        "https://source.unsplash.com/1024x768/?water",
        "https://source.unsplash.com/1024x768/?girl",
        "https://source.unsplash.com/1024x768/?tree",
    ]);




    const [coursesArr, setCoursesArr] = useState([]);
    const [happeningArr, setHappeningArr] = useState([]);
    const [topCategoriesArr, setTopCategoriesArr] = useState([]);
    const [topInstructorsArr, setTopInstructorsArr] = useState([]);





    const getInitialdata = async () => {
        try {
            // setIsLoading(true)
            let coursesRes = await getTopCourses()
            if (coursesRes.status == 200 || coursesRes.status == 304) {
                setCoursesArr(coursesRes.data.data.map(el => ({ ...el, uniqueId: nanoid() })))
            }

            let happeningtodayRes = await happeningTodayCourses()
            if (happeningtodayRes.status == 200 || happeningtodayRes.status == 304) {
                setHappeningArr(happeningtodayRes.data.data.map(el => ({ ...el, uniqueId: nanoid() })))
            }


            let topInstructorsRes = await getTopInstructors()
            if (topInstructorsRes.status == 200 || topInstructorsRes.status == 304) {
                setTopInstructorsArr(topInstructorsRes.data.data.map(el => ({ ...el, uniqueId: nanoid() })))
            }

            let CategoriesRes = await getAllCategories()
            if (CategoriesRes.status == 200 || CategoriesRes.status == 304) {

                let CategoriesArrTemp = CategoriesRes.data.data.map(el => ({ ...el, uniqueId: nanoid() })).filter(el => el.top == 1)

                setTopCategoriesArr(CategoriesArrTemp)
                console.log(JSON.stringify(CategoriesArrTemp, null, 2), "topCategoriesRes")
                // console.log(CategoriesArrTemp, "topCategoriesRes")
            }
            // console.log(topInstructorsRes, "topInstructorsRes")
            // setIsLoading(false)
        }
        catch (err) {
            // setIsLoading(false)
            console.warn(err, "asdas")
            console.log(JSON.stringify(err,null,2))
        }
    }


    useEffect(() => {
        if (isFocused) {
            getInitialdata()
        }
    }, [isFocused])







    const renderCourse = ({ item, index }) => {
        return (
            <Pressable style={styles.coursesCard} onPress={()=>props.navigation.navigate('SpecificClass',{courseObj:item})}>
                <Image source={{ uri: item.image }} style={styles.coursesCardImage} />
                <View style={[styles.flexColumn, { flex: 1, marginVertical: 15 }]}>
                    <View style={[styles.flexColumn, { alignSelf: "center", width: "78%" }]}>
                        <Text style={styles.courseName}>{item.title}</Text>
                        <Text style={styles.courseDescription}>by {item.instructor}</Text>
                        <Text style={styles.courseDescription}>{item.date}</Text>
                    </View>
                    <View style={{ alignItems: "center", display: "flex", flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
                        <Text style={styles.coursePrice}>{item.price}</Text>
                        <View style={[styles.flexRow, { alignItems: "center", justifyContent: "center", marginBottom: 6 }]}>
                            <Icon name="star" size={15} color={"#fcba05"} />
                            <Text> ({item.rating}) </Text>
                        </View>
                    </View>
                </View>
            </Pressable>
        )
    }

    const renderHappeningToday = ({ item, index }) => {
        return (

            <Pressable style={styles.coursesCard}>
                <Image source={{ uri: item.image }} style={styles.coursesCardImage} />
                <View style={[styles.flexColumn, { flex: 1, marginVertical: 15 }]}>
                    <View style={[styles.flexColumn, { alignSelf: "center", width: "78%" }]}>
                        <Text style={styles.courseName}>{item.title}</Text>
                        <Text style={styles.courseDescription}>by {item.instructor}</Text>
                        <Text style={styles.courseDescription}>{item.date}</Text>
                    </View>
                    <View style={{ alignItems: "center", display: "flex", flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
                        <Text style={styles.coursePrice}>{item.price}</Text>
                        <View style={[styles.flexRow, { alignItems: "center", justifyContent: "center", marginBottom: 6 }]}>
                            <Icon name="star" size={15} color={"#fcba05"} />
                            <Text> ({item.rating}) </Text>
                        </View>
                    </View>
                </View>
            </Pressable>

        )
    }

    const renderTopcategory = ({ item, index }) => {
        return (
            <Pressable style={styles.CategoryCard}>
                {

                    item.image ?
                        <Image source={{ uri: item.image }} style={styles.categoryCardImage} />
                        :
                        <Image source={require("../../assets/img/Yocolab-03.png")} resizeMethod="resize" resizeMode="cover" style={styles.categoryCardImage} />
                }
                <View style={styles.categoryDataContainer}>
                    <Text style={styles.CategoryName}>{item.name}</Text>
                    {/* <Text style={styles.categoryDescription}>{item.image}</Text> */}
                </View>
            </Pressable>
        )
    }


    const renderTopInstructor = ({ item, index }) => {
        var myloop = [];

        for (let i = 0; i < item.rating; i++) {
            myloop.push(
                <Icon name="star" key={i} size={15} color={"#fcba05"} />
            );
        }

        for (let i = 0; i < 5 - item.rating; i++) {
            myloop.push(
                <Icon name="star" key={`_${i}`} size={15} color={"#ccc"} />
            );
        }



        return (
            <Pressable style={styles.InstructorCard}>
                <View style={[styles.flexRow, { width: "100%", justifyContent: "space-between" }]}>
                    <Text style={styles.instructorName}>{item.name}</Text>

                    <View style={styles.flexRow}>

                        {myloop}

                    </View>
                </View>
                <View style={styles.flexRow}>
                    <Image source={{ uri: item.image }} style={styles.InstructorCardImage} />

                    <View style={[styles.flexColumn, { paddingHorizontal: 10, justifyContent: "space-between", flex: 1 }]}>
                        <View style={[styles.flexRow, { paddingLeft: 20, flex: 1, justifyContent: "space-between" }]}>
                            <Text style={styles.instructorCardHeading}>Experence</Text>
                            <Text style={styles.instructorExperience}>{item.experience}</Text>
                        </View>
                        <View style={[styles.flexRow, { paddingLeft: 20, flex: 1, justifyContent: "space-between" }]}>
                            <Text style={styles.instructorCardHeading}>Country</Text>
                            <Text style={styles.instructorCountry}>{item.country}</Text>

                        </View>
                        <View style={[styles.flexRow, { paddingLeft: 20, flex: 1, justifyContent: "space-between" }]}>
                            <Text style={styles.instructorCardHeading}>Expertise</Text>
                            <Text style={styles.instructorLanguage}>{item.expert}</Text>

                        </View>
                    </View>
                </View>
            </Pressable>
            // <Pressable style={styles.coursesCard}>
            //     <Image source={{ uri: item.image }} style={styles.coursesCardImage} />
            //     <View style={[styles.flexColumn, { flex: 1, marginVertical: 15 }]}>
            //         <View style={[styles.flexColumn, { alignSelf: "center", width: "78%" }]}>
            //             <Text style={styles.courseName}>{item.name}</Text>
            //             <Text style={styles.courseDescription}>Experience {item.experience}</Text>
            //             <Text style={styles.courseDescription}>Country: {item.country}</Text>
            //             <View style={[styles.flexRow,{paddingVertical:10}]}>

            //                 {myloop}

            //             </View>
            //         </View>
            //         {/* <View style={{ alignItems: "center", display: "flex", flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
            //           <Text style={styles.coursePrice}>{item.price}</Text>
            //           <View style={[styles.flexRow, { alignItems: "center", justifyContent: "center", marginBottom: 6 }]}>
            //               <Icon name="star" size={15} color={"#fcba05"} />
            //               <Text> ({item.rating}) </Text>
            //           </View>
            //       </View> */}
            //     </View>
            // </Pressable>

        )
    }



    return (
        <>
            <Header rootProps={props} name="Home" />
            <View contentContainerStyle={styles.container}>


                <View style={styles.dataContainer}>


                    <FlatList
                        data={coursesArr}
                        // horizontal
                        numColumns={2}
                        contentContainerStyle={{paddingBottom:60}}
                        scrollEnabled={true}
                        ListEmptyComponent={
                            <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
                        }
                        ListHeaderComponent={
                            <>
                                <SliderBox sliderBoxHeight={200} parentWidth={wp(100)} ImageComponentStyle={{}} inactiveDotColor="transparent" autoplay={true} circleLoop dotColor="transparent" images={images} />

                                <View style={[styles.flexRow, { justifyContent: "space-between", width: "100%", paddingHorizontal: 10, marginVertical: hp(2) }]}>
                                    <Text style={styles.SectionHeading}>Top Courses</Text>
                                    <Pressable style={styles.seeAllBtn} onPress={() => { console.log("asda") }}>
                                        <Text style={styles.seeAllBtnTxt}>See All</Text>
                                    </Pressable>
                                </View>
                            </>
                        }
                        keyExtractor={(item, index) => `${item.uniqueId}`}
                        renderItem={renderCourse}
                        ListFooterComponent={
                            <FlatList
                                data={happeningArr}
                                // contentContainerStyle={{ paddingBottom: 100 }}
                                numColumns={2}
                                scrollEnabled={false}

                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={
                                    <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
                                }
                                ListHeaderComponent={
                                    <>
                                        <SliderBox sliderBoxHeight={200} parentWidth={wp(100)} ImageComponentStyle={{}} inactiveDotColor="transparent" autoplay={true} circleLoop dotColor="transparent" images={images} />

                                        <View style={[styles.flexRow, { justifyContent: "space-between", width: "90%", alignSelf: "center", marginVertical: hp(2) }]}>
                                            <Text style={styles.SectionHeading}>Happening Today</Text>
                                            <Pressable style={styles.seeAllBtn} onPress={() => { console.log("asda") }}>
                                                <Text style={styles.seeAllBtnTxt}>See All</Text>
                                            </Pressable>
                                        </View>
                                    </>
                                }
                                keyExtractor={(item, index) => `${item.uniqueId}`}
                                renderItem={renderHappeningToday}


                                ListFooterComponent={
                                    <FlatList
                                        scrollEnabled={false}
                                        numColumns={2}
                                        columnWrapperStyle={{ justifyContent: 'space-between', paddingHorizontal: 20 }}
                                        ListEmptyComponent={
                                            <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
                                        }
                                        ListHeaderComponent={
                                            <View style={[styles.flexRow, { justifyContent: "space-between", width: "100%", paddingHorizontal: 10, marginVertical: hp(2) }]}>
                                                <Text style={styles.SectionHeading}>Top Categories</Text>
                                                <Pressable style={styles.seeAllBtn} onPress={() => { console.log("asda") }}>
                                                    <Text style={styles.seeAllBtnTxt}>See All</Text>
                                                </Pressable>
                                            </View>

                                        }



                                        ListFooterComponent={
                                            <FlatList
                                                scrollEnabled={false}
                                                // numColumns={2}
                                                ListHeaderComponent={
                                                    <>

                                                        <View style={[styles.flexRow, { justifyContent: "space-between", paddingHorizontal: 10, width: "100%", marginVertical: hp(2) }]}>
                                                            <Text style={styles.SectionHeading}>Top Instructors</Text>
                                                            <Pressable style={styles.seeAllBtn} onPress={() => { console.log("asda") }}>
                                                                <Text style={styles.seeAllBtnTxt}>See All</Text>
                                                            </Pressable>
                                                        </View>
                                                    </>
                                                }
                                                ListEmptyComponent={
                                                    <ActivityIndicator size="large" color={ColorObj.lightThemeButtonColor} />
                                                }
                                                data={topInstructorsArr}
                                                keyExtractor={(item, index) => `${item.uniqueId}`}
                                                renderItem={renderTopInstructor}
                                            />
                                        }


                                        data={topCategoriesArr}
                                        keyExtractor={(item, index) => `${item.uniqueId}`}
                                        renderItem={renderTopcategory}
                                    />
                                }

                            />


                        }
                    />











                </View>




            </View>
        </>
    )
}

const styles = StyleSheet.create({
    //////container
    container: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#FBFBFB",
        paddingBottom: 60
    },
    dataContainer: {
        width: wp(100),
        alignSelf: "center",
    },
    categoryDataContainer: {
        position: "absolute",
        bottom: 10,
        backgroundColor: "rgba(0,0,0,0.5)",
        paddingHorizontal: 5,
        paddingVertical: 5,
        borderRadius: 5,
        left: 10,
    },

    ///////card
    coursesCard: {
        width: "45%",
        // padding: 10,
        marginHorizontal: "2.5%",
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    CategoryCard: {
        width: wp(41),
        height: wp(41),
        // backgroundColor:'red',
        borderRadius: 15,
        display: "flex",
        position: "relative",
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    InstructorCard: {
        width: wp(89),
        borderRadius: 15,
        display: "flex",
        alignSelf: "center",
        padding: 25,
        marginVertical: hp(1.3),
        backgroundColor: "white",
        shadowColor: "rgba(0,0,0,0.7)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },



    /////////txt
    coursePrice: {
        fontFamily: "Montserrat-Bold",
    },
    instructorName: {
        fontFamily: "Montserrat-SemiBold",
        marginBottom: 18,
        textTransform: "capitalize",
        fontSize: 16
    },
    instructorCardHeading: {
        fontFamily: "Montserrat-Medium",
        color: "grey",
        marginRight: 30
    },
    instructorExperience: {
        fontFamily: "Montserrat-Regular",
        fontSize: 12
    },
    instructorCountry: {
        fontFamily: "Montserrat-Regular",
        fontSize: 12
    },
    instructorLanguage: {
        fontFamily: "Montserrat-Regular",
        fontSize: 12,
        width: "50%",
        textAlign: "right"
    },
    courseName: {
        fontFamily: "Montserrat-Bold",
        // width: "50%"
        fontSize: 13,
        marginBottom: 5
    },
    CategoryName: {
        fontFamily: "Montserrat-Bold",
        color: "white",
    },
    categoryDescription: {
        fontFamily: "Montserrat-Medium",
        color: "white",
        fontSize: 12
    },
    courseDescription: {
        fontFamily: "Montserrat-Medium",
        color: "grey",
        fontSize: 11,
        marginTop: 3
    },
    seeAllBtnTxt: {
        fontSize: 14,
        color: "grey",
        fontFamily: "Montserrat-Medium",
    },
    SectionHeading: {
        fontSize: 16,
        fontFamily: "Montserrat-Bold",
    },


    //////btn
    seeAllBtn: {
        backgroundColor: "transparent"
    },


    //////images
    coursesCardImage: {
        height: 125,
        // width: 60,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    categoryCardImage: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%",
        borderRadius: 15
    },
    InstructorCardImage: {
        height: 70,
        width: 70,
        borderRadius: 25
    },


    //////flex
    flexRow: {
        display: "flex",
        flexDirection: "row"
    },
    flexColumn: {
        display: "flex",
        flexDirection: "column"
    },
})