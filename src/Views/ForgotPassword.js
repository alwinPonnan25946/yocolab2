import React from 'react'
import { View, Text, Image, StyleSheet, Pressable, ImageBackground, TextInput, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome";
import ColorObj from "../Globals/Colors"

export default function ForgotPassword() {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.headingContainer}>
                <Text style={styles.Heading}>Reset your </Text>
                <Text style={styles.Heading}> Password </Text>
            </View>
            <View style={styles.flexColumn}>
                <Text style={styles.label}>Email</Text>
                <TextInput placeholder="Email" placeholderTextColor="rgba(0,0,0,0.2)" style={styles.TextInputStyles} />
            </View>
            <Text style={styles.noteTxt}>We'll send an otp on this number,Do not share this otp with anyone </Text>
            <Pressable style={styles.btn}>
                <Text style={styles.btnTxt}>
                    Submit
                </Text>
            </Pressable>

        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorObj.lightThemeGreyBgColor,
        flex: 1
    },
    headingContainer: {
        backgroundColor: ColorObj.lightThemeBg1Color,
        height: wp(40),
        width: wp(100),
        display: "flex",
        justifyContent: "center",
        borderBottomRightRadius: 60,
        paddingHorizontal: wp(10),
    },
    Heading: {
        fontFamily: "Montserrat-Medium",
        color: ColorObj.lightThemeBgColor,
        fontSize: 28
    },

    flexRow: {
        display: "flex",
        flexDirection: "row",
    },

    flexColumn: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: hp(10)
    },

    label: {
        fontFamily: "Montserrat-Medium",
        marginVertical: 10,
        textAlign: "left",
        width: wp(80),
        paddingLeft: 10,
        marginTop: 25,
        color: "rgba(0,0,0,0.3)"
    },
    noteTxt: {
        fontFamily: "Montserrat-Medium",
        marginTop: 10,
        textAlign: "left",
        alignSelf: "center",
        fontSize: 12,
        width: wp(80),
        // paddingLeft: 10,
        color: "rgba(0,0,0,0.3)"
    },

    TextInputStyles: {
        width: wp(80),
        borderRadius: 15,
        paddingHorizontal: 15,
        shadowColor: ColorObj.lightThemeBg1Color,
        color: "rgba(0,0,0,0.2)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
        backgroundColor: ColorObj.lightThemeBgColor,
    },
    btn: {
        width: wp(80),
        backgroundColor: ColorObj.lightThemeButtonColor,
        height: 45,
        borderRadius: 15,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(5),
    },
    forgotBtn: {
        width: wp(80),
        backgroundColor: "transparent",
        height: 45,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        marginTop: hp(3),
    },
    forgotBtnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeTextColor
    },
    btnTxt: {
        fontFamily: "Montserrat-Medium",
        fontSize: 17,
        color: ColorObj.lightThemeBgColor
    },

    socialMediaBtn: {
        backgroundColor: ColorObj.lightThemeBgColor,
        borderRadius: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        height: 40, width: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    AccentColoredTxt: {
        fontFamily: "Montserrat-Regular",
        color: ColorObj.lightThemeBg1Color
    },
    getStartedBtnTxt: {
        fontFamily: "Montserrat-Bold",
        color: ColorObj.lightThemeBg1Color
    },
})