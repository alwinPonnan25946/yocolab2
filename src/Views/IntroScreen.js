import React from 'react'
import { View, Text, Image, StyleSheet, Pressable, ImageBackground } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AppIntroSlider from 'react-native-app-intro-slider';
import Images from '../Globals/Images';
import Icon from "react-native-vector-icons/FontAwesome";
import ColorObj from "../Globals/Colors"
export default function IntroScreen(props) {

    const slides = [
        {
            index: 1,
            title: 'Learn Anything',
            text: 'There are no barriers, no boundaries when it comes to learning. Pick your favorite topic, add new skills and be creative.',
            image: Images.intro1,
            backgroundColor: "transparent"
        },
        {
            index: 2,
            title: 'Learn Anytime',
            text: 'Classes round-the-clock. Learn as per your convenience, revel in the pleasure of learning.',
            image: Images.intro2,
            backgroundColor: "transparent"

        },
        {
            index: 3,
            title: 'Be An Instructor',
            text: 'You can make a difference. Teaching is one of the most direct ways to make an impact. Become a Guru',
            image: Images.intro3,
            backgroundColor: "transparent"

        },

    ];

    const renderSlide = ({ item, index }) => {
        return (
            <ImageBackground source={item.image} style={{ height: hp(100), width: wp(100) }} >
                <View style={styles.card}>
                    <View style={styles.cardContainer}>
                    </View>
                    <View style={styles.cardInnerContainer}>
                        <Text style={styles.Title}>{item.title}</Text>
                        <Text style={styles.text}>{item.text}</Text>
                    </View>
                </View>
            </ImageBackground>
        )
    }

    const onDone = () => {
        return (
            props.navigation.navigate("Login")
        )
    }

    return (
        <AppIntroSlider renderItem={renderSlide} data={slides} onDone={onDone} />
    )
}

const styles = StyleSheet.create({
    doneBtn: {
        height: 70,
        width: 70,
        backgroundColor: "white",
        borderRadius: 50,
    },
    card: {
        width: wp(80),
        height: 200,
        backgroundColor: "white",
        position: "relative",
        borderRadius: 10,
        display: "flex",
        alignSelf: "center",
        justifyContent: "center",
        marginTop: hp(28),
        zIndex: 10
    },
    cardContainer: {
        backgroundColor: ColorObj.lightThemeButtonColor,
        width: wp(40),
        position: "absolute",
        top: -7,
        left: -10,
        borderRadius: 5,
        height: 160,
        zIndex: -10
    },
    cardInnerContainer: {
        height: "100%",
        width: "100%",
        borderRadius: 10,
        backgroundColor: "white",
        padding: 15
    },
    Title: {
        fontFamily: "Montserrat-ExtraBold",
        fontSize: 20,
        marginBottom: 15,
        color: "black",
    },
    text: {
        fontFamily: "Montserrat-Regular",
        fontSize: 17,
        lineHeight: 30,
        color: "black",
    },
})