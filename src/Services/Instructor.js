import axios from 'axios'

import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'
import { decodeJwt, getJwt } from "./user";
import moment from 'moment';
import 'moment-timezone';

export const uploadStudyMaterialInstructor = async (formData) => {
    try {

        let res = await axios.post(`${apiUrl}/instructor/material/create`, formData)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}

export const getTopInstructors = async () => {
    try {

        let timeZone = moment.tz.guess();

        let config = {
            headers: {
                'timezone': `${timeZone}`
            }
        }

        let res = await axios.get(`${apiUrl}/instructors`, config)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}


export const becomeInstructor = async (formData) => {
    try {

        let res = await axios.post(`${apiUrl}/student/instructor-register`, formData)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}






export const createCreateClass = async (formData) => {
    try {

        let res = await axios.post(`${apiUrl}/instructor/create-class`, formData)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}


export const getStudyMaterialsInstructor = async () => {
    try {


        let res = await axios.get(`${apiUrl}/instructor/material`);

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}


export const getInstructorData = async () => {
    try {

        let res = await axios.get(`${apiUrl}/instructor/profile`);

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}


export const getSpecificInstructor = async (slug) => {
    let res = await axios.get(`${apiUrl}/instructor-profile/${slug}`);
    return res
}

