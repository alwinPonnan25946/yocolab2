import axios from 'axios'
import jwt_decode from "jwt-decode";
import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'



export const userLogin = async (formData) => {
    try {
        let res = await axios.post(`${apiUrl}/login`, formData)
        return res
    } catch (error) {
        console.error(error)
        throw (error)
    }
}

export const userRegister = async (formData) => {
    try {
        let res = await axios.post(`${apiUrl}/register`, formData)
        return res
    } catch (error) {
        console.error(error)
        throw (error)

    }
}

export const getUserRoleData = async () => {
    try {
        let res = await axios.get(`${apiUrl}/role`)
        return res
    } catch (error) {
        console.error(error)
        throw (error)

    }
}

export const setJwt = async (token) => {
    let tokenCheck = await EncryptedStorage.setItem('AUTH_TOKEN', token)
    if (tokenCheck)
        return true
    else
        return false
}





export const setRoleToStorage = async (role) => {
    let tokenCheck = await EncryptedStorage.setItem('role', role)
    if (tokenCheck)
        return true
    else
        return false
}
export const removeRole = async () => {
    let token = await EncryptedStorage.removeItem('role')
    if (token)
        return true
    else
        return false
}

export const getUserRole = async () => {
    let token = await EncryptedStorage.getItem('role')
    return token
}




export const getJwt = async () => {
    let token = await EncryptedStorage.getItem('AUTH_TOKEN')
    return token
}






export const decodeJwt = async () => {
    let token = await EncryptedStorage.getItem('AUTH_TOKEN')
    var decoded = jwt_decode(token);
    return decoded
}

export const removeJwt = async () => {
    let token = await EncryptedStorage.removeItem('AUTH_TOKEN')
    if (token)
        return true
    else
        return false
}


export const addClassToWishlist=async(id)=>{
    return await axios.get(`${apiUrl}/student/wishlist/add/${id}`)
}




