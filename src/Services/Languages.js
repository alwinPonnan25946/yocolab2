import axios from 'axios'

import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'



export const getAllLanguages = async () => {
    try {

        let res = await axios.get(`${apiUrl}/language`)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}