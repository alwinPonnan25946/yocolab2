import axios from 'axios'

import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'
import { decodeJwt, getJwt } from "./user";


export const getStudentData = async () => {
    try {
        let res = await axios.get(`${apiUrl}/student/profile`);
        return res
    } catch (error) {
        console.error(error)
        throw (error)
    }
}
