import axios from 'axios'

import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'



export const getAllCountries = async () => {
    try {

        let res = await axios.get(`${apiUrl}/country`)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}