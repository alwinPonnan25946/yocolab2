import axios from 'axios'

import { apiUrl } from '../Globals/url'
import EncryptedStorage from 'react-native-encrypted-storage'
import { decodeJwt, getJwt } from "./user";
import moment from 'moment';
import 'moment-timezone';





export const happeningTodayCourses = async () => {
    try {

        let timeZone = moment.tz.guess();

        let config = {
            headers: {
                'timezone': `${timeZone}`
            }
        }

        let res = await axios.get(`${apiUrl}/today-courses`, config)

        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}

export const getTopCourses = async () => {
    try {

        let timeZone = moment.tz.guess();

        let config = {
            headers: {
                'timezone': `${timeZone}`
            }
        }

        let res = await axios.get(`https://yocolab.com/api/top-courses`, config)
        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}


export const myCourses = async () => {
    try {



        let res = await axios.get(`${apiUrl}/student/my-courses`)
        return res

    } catch (error) {
        console.error(error)
        throw (error)
    }
}



export const getWishlist = async () => {
    return await axios.get(`${apiUrl}/student/wishlist`)
}



export const getSpecificClassBySlug = async (slug) => {
    return await axios.get(`${apiUrl}/class/${slug}`)
}

export const removeCourseFromWishlist = async (id) => {
    return await axios.get(`${apiUrl}/student/wishlist/remove/${id}`)
}

