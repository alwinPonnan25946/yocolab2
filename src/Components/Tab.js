///////////react native default imports
import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

////////////external packages which are installed are here
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import ColorObj from '../Globals/Colors';

export const Tab = ({ color, tab, onPress, icon, backgroundColor, isFocused, ...rest }) => {

    return (
        <TouchableOpacity style={{ display: "flex", justifyContent: "center", flex: 1, alignItems: "center", flexDirection: "column" }} onPress={onPress}>
            <Icon
                name={icon}
                size={22}
                color={color}
                backgroundColor="transprent"
            />
            {/* <Text style={{ marginTop: 5, color: isFocused ? ColorObj.lightThemeButtonColor : "black", fontFamily: "Montserrat-Regular" }}></Text> */}
        </TouchableOpacity>
    )
}

export default Tab;
