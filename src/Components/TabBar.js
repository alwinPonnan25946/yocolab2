///////////react native default imports
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions, Keyboard } from 'react-native'

////////////external packages which are installed are here
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


////////////component import
import Tab from './Tab';
import ColorObj from '../Globals/Colors';
//////////////utilities like styles and images are imported here
// import colors from "../utilities/Colors";

export const TabBar = ({ state, navigation, label }) => {

    const [isKeyboardVisible, setIsKeyboardVisible] = useState(false);


    const { routes } = state;


    const setColor = currentTab => {
        return currentTab === state.index ? ColorObj.lightThemeButtonColor : "grey"
    }

    const setBackgroundColor = currentTab => {
        return currentTab === state.index ? ColorObj.lightThemeButtonColor : "transparent"
    }


    const setselectedTab = activeTab => {
        let obj = {}

        if (activeTab == "HomeDrawer") {
            obj = { screen: 'HomeDrawer' }
        }
        else if (activeTab == "Search") {
            obj = { screen: 'Search' }
        }
        else if (activeTab == "Upcoming") {
            obj = { screen: 'Upcoming' }
        }
        else if (activeTab == "WishList") {
            obj = { screen: 'WishList' }
        }
        else if (activeTab == "Profile") {
            obj = { screen: 'Profile' }
        }

        obj.activeTab = activeTab
        navigation.navigate(activeTab, obj)
    }



    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setIsKeyboardVisible(true);
            },
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setIsKeyboardVisible(false);
            },
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, [])

    return (
        <>
            {
                !isKeyboardVisible ?
                    <View style={styles.wrapper}>
                        <View style={styles.container}>
                            {
                                routes.map((route, index) => {

                                    const isFocused = state.index == index

                                    return (<Tab
                                        tab={route}
                                        isFocused={isFocused}
                                        icon={route.params.icon}
                                        color={setColor(index)}
                                        backgroundColor={setBackgroundColor(index)}
                                        key={route.key}
                                        onPress={() => { setselectedTab(route.name); }}
                                    />)
                                })
                            }
                        </View>
                    </View>
                    : <></>
            }
        </>
    )
}
const styles = StyleSheet.create({
    wrapper: {
        // position: "absolute",
        // bottom: 0,
        // left: 0,
        
        width: wp(100),
        // right: 0,
        backgroundColor: "#ffffff", ///////tab navigator background color
        height: 50,
        borderRadius: 0,


        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,

        elevation: 16,


    },
    container: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        height: "100%",
    },
})

export default TabBar;
