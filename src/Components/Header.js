import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, Pressable, ImageBackground } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome5"
import { DrawerActions } from '@react-navigation/native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default function Header(props) {

    const toggle = () => {
        props.rootProps.navigation.dispatch(DrawerActions.toggleDrawer())
    }

    return (
        <>
            <View style={styles.container}>
                <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                    <Pressable style={styles.btn} onPress={() => toggle()}>
                        <Image source={require("../../assets/img/menu.png")} style={styles.icons} />
                    </Pressable>
                    <Text style={styles.headerTitle}>{props?.name}</Text>
                    <Pressable onPress={() => toggle()} style={[styles.btn]}>
                        <Image source={require('../../assets/img/notification.png')} style={styles.bellIcon} />
                    </Pressable>
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        width: '100%',
        fontFamily: 'Montserrat-Black',
        fontSize: 32
    },
    icons: {
        height: 30,
        width: 30,
        margin: 10
    },
    bellIcon: {
        height: 20,
        width: 20,
        margin: 10
    },
    headerTitle: {
        fontFamily: 'Montserrat-SemiBold',
        marginTop: 10
    },
    flexRow: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },

});