
import React, { createContext, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  StyleSheet,
} from 'react-native';
import RootStack from './src/Navigation/Stack/RootStack';
// import { Provider as PaperProvider } from 'react-native-paper';
import Loading from './src/Views/Loading';
import Toast from 'react-native-toast-message';

const App = () => {

  return (
    <PaperProvider>

      <NavigationContainer>
        <RootStack />
      </NavigationContainer>
      {
        isLoading &&
        <Loading />
      }
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </PaperProvider>
  );
};

const styles = StyleSheet.create({

});

export default App;
